﻿/*

*/

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;

namespace EA.Net
{
    [Serializable()]
    public class DEConfig
    {
        private bool mAlterado;
        private bool mFlagAutoNP = true;
        private int mNP = 50;
        private int mMaxGEN = 100;
        private double mCR = 0.9;

        private double mF = 0.8;
        private Vars.eEstrategiaDE mEstrategia;

        public Vars.eEstrategiaDE Estrategia
        {
            get { return mEstrategia; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mEstrategia); }
        }

        public bool FlagAutoNP
        {
            get { return mFlagAutoNP; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mFlagAutoNP); }
        }

        public int NP
        {
            get { return mNP; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mNP); }
        }

        public int MaxGEN
        {
            get { return mMaxGEN; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mMaxGEN); }
        }

        public double CR
        {
            get { return mCR; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mCR); }
        }

        public double F
        {
            get { return mF; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mF); }
        }

       

        private delegateFO _CalculaFO;

          [Serializable()]
        public delegate double delegateFO(double[] VetorX);

        [System.Xml.Serialization.XmlIgnore]
        public delegateFO CalculaFO
        {
            get { return _CalculaFO; }
            set { _CalculaFO = value; }
        }

        private double _DeltaMin;
        public double DeltaMin
        {
            set { _DeltaMin = value; }
            get { return _DeltaMin; }
        }
    }

    [Serializable()]
    public class DE : Util
    {
        private DEConfig _DEConfig;
        private Random rnd = new Random();

        private int _NVars;
        private double[] _XLower;
        private double[] _XUpper;
        private List<double> _FOList = new List<double>();

        public List<double> FOlist
        {
            set { _FOList = value; }
            get { return _FOList; }
        }

        public DEConfig DEConfig
        {
            set { _DEConfig = value; }
            get { return _DEConfig; }
        }

        public int NVars
        {
            set { _NVars = value; }
            get { return _NVars; }
        }

        public double[] XLower
        {
            set { _XLower = value; }
            get { return _XLower; }
        }

        public double[] XUpper
        {
            set { _XUpper = value; }
            get { return _XUpper; }
        }

        public DE()
        { }

        public void Optimize()
        {
            BackgroundWorker bckProcess;
            bckProcess = null;

            Optimize(ref bckProcess);
        }

        public double[] vMelhor;
        
        public void Optimize(ref BackgroundWorker bckProcess)
        {
            int r1 = 0;
            int r2 = 0;
            int r0 = 0;
            int r3, r4;

            int indMinGlobal = 0;
            int cntGER = 0;
            int indMinNew = 0;

            double[] vNovo;
            double[] vFO;
            double[][] vX;
            double[][] vAux;
            double FOMinGlobal = 0;
            double FOAux = 0;

            if (_DEConfig.FlagAutoNP)
                _DEConfig.NP = 20 * _NVars;

            vMelhor = new double[_NVars];

            vNovo = new double[_NVars];
            vFO = new double[_DEConfig.NP];

            vX = new double[_DEConfig.NP][];
            vAux = new double[_DEConfig.NP][];

            for (int i = 0; i <= _DEConfig.NP - 1; i++)
            {
                vX[i] = new double[_NVars];
                vAux[i] = new double[_NVars];
                for (int j = 0; j <= _NVars - 1; j++)
                {
                    vX[i][j] = _XLower[j] + RND.rndDouble() * (_XUpper[j] - _XLower[j]);
                }
                vFO[i] = _DEConfig.CalculaFO(vX[i]);
            }

            FOMinGlobal = vFO[0];
            indMinGlobal = 0;
            for (int i = 1; i <= _DEConfig.NP - 1; i++)
            {
                if (vFO[i] < FOMinGlobal)
                {
                    FOMinGlobal = vFO[i];
                    indMinGlobal = i;
                }
            }

            copyPoint(vX[indMinGlobal], ref vMelhor);
            //criterio de parada
            int cntNoAcc = 0;

            while (cntGER < _DEConfig.MaxGEN)
            {
                for (int i = 0; i <= _DEConfig.NP - 1; i++)
                {
                    do
                        r0 = (int)Math.Floor(RND.rndDouble() * _DEConfig.NP);
                    while (r0 == i);
                    do
                        r1 = (int)Math.Floor(RND.rndDouble() * _DEConfig.NP);
                    while (r1 == i | r1 == r0);
                    do
                        r2 = (int)Math.Floor(RND.rndDouble() * _DEConfig.NP);
                    while (r2 == i | r2 == r1 | r2 == r0);
                    do
                        r3 = (int)Math.Floor(RND.rndDouble() * _DEConfig.NP);
                    while (r3 == i || r3 == r2 || r3 == r1 || r3 == r0);
                    do
                        r4 = (int)Math.Floor(RND.rndDouble() * _DEConfig.NP);
                    while (r4 == i || r4 == r3 || r4 == r2 || r4 == r1 || r4 == r0);


                    vNovo = new double[NVars];
                    int jRnd = (int) (RND.rndDouble() * (NVars - 1));

                    //--------------- DE/best/1/bin ---------------------
                    if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_best_1_bin)
                    {
                        for (int j = 0; j <= NVars - 1; j++)
                        {
                            if (RND.rndDouble() <= _DEConfig.CR || j == NVars - 1)
                            {
                                vNovo[jRnd] = vMelhor[jRnd] + _DEConfig.F * (vX[r0][jRnd] - vX[r1][jRnd]);
                            }
                            else
                                vNovo[jRnd] = vX[i][jRnd];

                            jRnd = (jRnd + 1) % NVars;
                        }
                    }

                    //--------------- DE/rand/1/bin ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_rand_1_bin)
                    {
                        for (int j = 0; j <= _NVars - 1; j++)
                        {
                            if (RND.rndDouble() < _DEConfig.CR || j == NVars - 1)
                                vNovo[jRnd] = vX[r2][jRnd] + _DEConfig.F * (vX[r0][jRnd] - vX[r1][jRnd]);
                            else
                                vNovo[jRnd] = vX[i][jRnd];
                            jRnd = (jRnd + 1) % _NVars;
                        }
                    }

                    /*--------------- DE/best/2/bin ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_best_2_bin)
                    {
                        for (int j = 0; j <= _NVars - 1; j++)
                        {
                            if (RND.rndDouble() < _DEConfig.CR || j == _NVars - 1)
                                vNovo[jRnd] = vMelhor[jRnd] + _DEConfig.F * (vX[r0][jRnd] + vX[r1][jRnd] - vX[r2][jRnd] - vX[r3][jRnd]);
                            else
                                vNovo[jRnd] = vX[i][jRnd];

                            jRnd = (jRnd + 1) % _NVars;
                        }
                    }

                    /*--------------- DE/rand/2/bin ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_rand_2_bin)
                    {
                        for (int j = 0; j <= _NVars - 1; j++)
                        {
                            if (RND.rndDouble() < _DEConfig.CR || j == _NVars - 1)
                                vNovo[j] = vX[r4][j] + _DEConfig.F * (vX[r0][j] + vX[r1][j] - vX[r2][j] - vX[r3][j]);
                            else
                                vNovo[jRnd] = vX[i][jRnd];

                            jRnd = (jRnd + 1) % _NVars;
                        }
                    }

                    /*--------------- DE/rand-to-best/1/bin ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_rand_to_best_1_bin)
                    {
                        for (int j = 0; j <= _NVars - 1; j++)
                        {
                            if (RND.rndDouble() < _DEConfig.CR || j == _NVars - 1)
                                vNovo[j] = vNovo[j] + _DEConfig.F * (vMelhor[j] - vNovo[j]) + _DEConfig.F * (vX[r0][j] - vX[r1][j]);
                            else
                                vNovo[j] = vX[i][j];

                            j = (j + 1) % _NVars;
                        }
                    }

                    /*--------------- DE/rand/1/exp ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_rand_1_exp)
                    {
                        int j = 0;
                        do
                        {
                            vNovo[jRnd] = vX[r2][jRnd] + _DEConfig.F * (vX[r0][jRnd] - vX[r1][jRnd]);

                            jRnd = (jRnd + 1) % _NVars;
                            j++;
                        }
                        while (RND.rndDouble() < _DEConfig.CR && j < _NVars);
                    }

                    /*--------------- DE/best/1/exp ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_best_1_exp)
                    {
                        int j = 0;
                        do
                        {
                            vNovo[j] = vMelhor[j] + _DEConfig.F * (vX[r0][j] - vX[r1][j]);

                            j = (j + 1) % NVars;
                            j++;
                        }
                        while (RND.rndDouble() < _DEConfig.CR && j < _NVars);
                    }

                    /*--------------- DE/best/2/exp ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_best_1_exp)
                    {

                        int j = 0;
                        do
                        {
                            vNovo[jRnd] = vMelhor[jRnd] + _DEConfig.F * (vX[r0][jRnd] + vX[r1][jRnd] - vX[r2][jRnd] - vX[r3][jRnd]);

                            jRnd = (jRnd + 1) % NVars;
                            j++;
                        }
                        while (RND.rndDouble() < _DEConfig.CR && j < NVars);
                    }

                    /*--------------- DE/rand/2/exp ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_rand_2_exp)
                    {

                        int j = 0;
                        do
                        {
                            vNovo[jRnd] = vX[r4][jRnd] + _DEConfig.F * (vX[r0][jRnd] + vX[r1][jRnd] - vX[r2][jRnd] - vX[r3][jRnd]);
                            jRnd = (jRnd + 1) % NVars;
                            j++;
                        }
                        while (RND.rndDouble() < _DEConfig.CR && j < NVars);
                    }

                    /*--------------- DE/rand-to-best/1/exp ---------------------*/
                    else if (_DEConfig.Estrategia == Vars.eEstrategiaDE.DE_rand_to_best_1_exp)
                    {

                        int j = 0;
                        do
                        {
                            vNovo[jRnd] = vNovo[jRnd] + _DEConfig.F * (vMelhor[jRnd] - vNovo[jRnd]) + _DEConfig.F * (vX[r0][jRnd] - vX[r1][jRnd]);

                            jRnd = (jRnd + 1) % NVars;
                            j++;
                        }
                        while (RND.rndDouble() < _DEConfig.CR && j < NVars);
                    }

                    for (int j = 0; j < vNovo.Length; j++)
                    {
                        if (vNovo[j] > _XUpper[j])
                            vNovo[j] = _XUpper[j];
                        else if (vNovo[j] < _XLower[j])
                            vNovo[j] = _XLower[j];

                    }

                    FOAux = _DEConfig.CalculaFO(vNovo);
                    if (FOAux < vFO[i])
                    {
                        copyPoint(vNovo, ref vAux[i]);
                        vFO[i] = FOAux;
                        cntNoAcc = 0;
                    }
                    else
                    {
                        copyPoint(vX[i], ref vAux[i]);
                        cntNoAcc += 1;
                    }

                    //if (vFO[i] - FOAux > _DEConfig.DeltaMin)
                    //    cntNoAcc = 0;
                    //else
                    //    cntNoAcc += 1;
                }

                for (int i = 0; i <= _DEConfig.NP - 1; i++)
                {
                    copyPoint(vAux[i], ref vX[i]);
                }

                indMinNew = -1;
                for (int i = 1; i <= _DEConfig.NP - 1; i++)
                {
                    if (vFO[i] < FOMinGlobal)
                    {
                        FOMinGlobal = vFO[i];
                        indMinNew = i;
                    }
                }


                if (indMinNew > -1)
                {
                    copyPoint(vX[indMinNew], ref vMelhor);
                }

                _FOList.Add(FOMinGlobal);

                cntGER += 1;

                if (bckProcess != null)
                    bckProcess.ReportProgress( (int)((float)cntGER / _DEConfig.MaxGEN * 100), cntGER);

                if (FOMinGlobal == 0)
                    break;
            }

            _DEConfig.CalculaFO(vMelhor);
        }

        private void copyPoint(double[] trial, ref double[] x)
        {
            for (int i = 0; i <= trial.GetUpperBound(0); i++)
                x[i] = trial[i];
            //trial.CopyTo(x, 0);
        }

    }
}


