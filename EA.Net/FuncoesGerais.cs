﻿using System;
namespace EA.Net
{
    public static class FuncoesGerais
    {
        #region " Private functions "

        public static void ObjMaxMin(ref double[] ObjMax, ref double[] ObjMin, Population p)
        {
            int NrObj = p.Ind[0].Objective.Length;
            ObjMax = new double[NrObj];
            ObjMin = new double[NrObj];
            Individual[] ind = new Individual[p.Ind.Length];
            Array.Copy(p.Ind, ind, p.Ind.Length);

            for (int i = 0; i < NrObj; i++)
            {
                SortObjectiveComparer comp = new SortObjectiveComparer(i);
                Array.Sort(ind, comp);
                ObjMin[i] = ind[0].Objective[i];
                ObjMax[i] = ind[ind.Length - 1].Objective[i];
            }

        }

        public static void Validar(ref bool mAlterado, Vars.eEstrategiaDE ValorNovo, ref Vars.eEstrategiaDE ValorAntigo)
        {
            if (!ValorNovo.Equals(ValorAntigo))
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;
            }
        }

        public static void Validar(ref bool mAlterado, Vars.eTipoFO ValorNovo, ref Vars.eTipoFO ValorAntigo)
        {
            if (!ValorNovo.Equals(ValorAntigo))
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;
            }
        }

        public static void Validar(ref bool mAlterado, double[] ValorNovo, ref double[] ValorAntigo)
        {
            if (!ValorNovo.Equals(ValorAntigo))
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;
            }
        }

        public static void Validar(ref bool mAlterado, System.DateTime ValorNovo, ref System.DateTime ValorAntigo)
        {
            if (ValorNovo != ValorAntigo)
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;

            }
        }

        public static void Validar(ref bool mAlterado, Vars.eTipoModelo ValorNovo, ref Vars.eTipoModelo ValorAntigo)
        {
            if (!ValorNovo.Equals(ValorAntigo))
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;

            }
        }

        public static void Validar(ref bool mAlterado, double ValorNovo, ref double ValorAntigo)
        {
            if (ValorNovo != ValorAntigo)
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;
            }
        }

        public static void Validar(ref bool mAlterado, int ValorNovo, ref int ValorAntigo)
        {
            if (ValorNovo != ValorAntigo)
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;
            }
        }

        public static void Validar(ref bool mAlterado, DEConfig ValorNovo, ref DEConfig ValorAntigo)
        {
            if (ValorAntigo == null || ValorAntigo.Equals(ValorNovo))
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;

            }
        }

        public static void Validar(ref bool mAlterado, string ValorNovo, ref string ValorAntigo)
        {
            if (!ValorAntigo.Equals(ValorNovo))
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;

            }
        }

        public static void Validar(ref bool mAlterado, bool ValorNovo, ref bool ValorAntigo)
        {
            if (ValorAntigo != ValorNovo)
            {
                ValorAntigo = ValorNovo;
                mAlterado = true;
            }
        }
        #endregion
    }
}