﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class Util
    {
        public double EPS = 0;

        public int ChekDominace(Individual a, Individual b)
        {
            return ChekDominace(a, b, _EAConfig.NumberObjectives);
        }

        public int ChekDominace(Individual a, Individual b, int NumberObjectives)
        {
            int i;
            int flag1 = 0;
            int flag2 = 0;

            if (a.ConstraintViolation > 0 && b.ConstraintViolation > 0)
            {
                if (a.ConstraintViolation < b.ConstraintViolation)
                    return 1;
                else
                {
                    if (a.ConstraintViolation > b.ConstraintViolation)
                        return -1;
                    else
                        return 0;
                }
            }
            else
            {
                if (a.ConstraintViolation > 0 && b.ConstraintViolation == 0)
                    return -1;
                else
                {
                    if (a.ConstraintViolation == 0 && b.ConstraintViolation > 0)
                        return 1;
                    else
                    {
                        for (i = 0; i < NumberObjectives; i++)
                        {
                            if (a.Objective[i] + EPS < b.Objective[i])
                                flag1 = 1;
                            else if (a.Objective[i] + EPS > b.Objective[i])
                                flag2 = 1;
                        }
                        if (flag1 == 1 && flag2 == 0)
                            return 1;
                        else
                            if (flag1 == 0 && flag2 == 1)
                                return -1;
                            else
                                return 0;
                    }
                }
            }
        }
    }
}