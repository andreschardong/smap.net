﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EA.Net
{
    public partial class Util
    {

        /* Routine to compute crowding distance based on ojbective function values when the population in in the form of a list */
        
        public void AssignRankCrowdingDistance(ref Population Pop)
        {
            LstUtil = new ListEAUtil();            
            int Flag, End, FrontSize = 0, Rank = 0;

            NodeEA Orig = new NodeEA();
            NodeEA Cur = new NodeEA();
            NodeEA Temp1, Temp2;

            Orig.Index = -1;
            Orig.Parent = null;
            Orig.Child = null;
            Cur.Index = -1;
            Cur.Parent = null;
            Cur.Child = null;
            Temp1 = Orig;

            for (int i = 0; i < _EAConfig.PopSize; i++)
            {
                LstUtil.Insert(ref Temp1, i);
                Temp1 = Temp1.Child;
            }
            do
            {
                if (Orig.Child.Child == null)
                {
                    Pop.Ind[Orig.Child.Index].Rank = Rank;
                    Pop.Ind[Orig.Child.Index].CrowdingDistance = double.PositiveInfinity;
                    break;
                }

                Temp1 = Orig.Child;
                LstUtil.Insert(ref Cur, Temp1.Index);
                FrontSize = 1;
                Temp2 = Cur.Child;
                Temp1 = LstUtil.Delete(Temp1);
                Temp1 = Temp1.Child;

                do
                {
                    Temp2 = Cur.Child;
                    do
                    {
                        End = 0;
                        Flag = ChekDominace(Pop.Ind[Temp1.Index], Pop.Ind[Temp2.Index]);
                        if (Flag == 1)
                        {
                            LstUtil.Insert(ref Orig, Temp2.Index);
                            Temp2 = LstUtil.Delete(Temp2);
                            FrontSize--;
                            Temp2 = Temp2.Child;
                        }
                        else if (Flag == 0)
                            Temp2 = Temp2.Child;

                        else if (Flag == -1)
                            End = 1;
                    }
                    while (End != 1 && Temp2 != null);

                    if (Flag == 0 || Flag == 1)
                    {
                        LstUtil.Insert(ref Cur, Temp1.Index);
                        FrontSize++;
                        Temp1 = LstUtil.Delete(Temp1);
                    }
                    Temp1 = Temp1.Child;
                }
                while (Temp1 != null);

                Temp2 = Cur.Child;
                do
                {
                    Pop.Ind[Temp2.Index].Rank = Rank;
                    Temp2 = Temp2.Child;
                }
                while (Temp2 != null);

                AssignCrowdingDistanceList(ref Pop, Cur.Child, FrontSize);
                Temp2 = Cur.Child;
                do
                {
                    Temp2 = LstUtil.Delete(Temp2);
                    Temp2 = Temp2.Child;
                }
                while (Cur.Child != null);

                Rank += 1;
            }
            while (Orig.Child != null);
        }
    }
}