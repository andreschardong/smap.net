﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Xml.Serialization;

namespace EA.Net
{
    [Serializable]
    public class Individual
    {
        public int Rank;
        public double ConstraintViolation;
        public double[] Xreal;
        public byte[] Xbin;
        public double[] Objective;
        public double XComp;
        public double YComp;
        public double CrowdingDistance;
        public double RankCompro = -1;
    }
}
