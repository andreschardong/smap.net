﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Xml.Serialization;

namespace EA.Net
{
    public class SortCDComparer : System.Collections.Generic.IComparer<Individual>
    {
        public int Compare(Individual first, Individual second)
        {
            if (first.CrowdingDistance < second.CrowdingDistance)
                return -1;
            else if (first.CrowdingDistance == second.CrowdingDistance)
                return 0;
            else
                return 1;
        }
    }

    public class SortObjectiveComparer : System.Collections.Generic.IComparer<Individual>
    {
        private int mObj;
        public SortObjectiveComparer(int obj)
        {
            mObj = obj;
        }
        public int Compare(Individual first, Individual second)
        {
            if (first.Objective[mObj] < second.Objective[mObj])
                return -1;

            else if (first.Objective[mObj] == second.Objective[mObj])
                return 0;
            else
                return 1;
        }
    }

}
