﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Xml.Serialization;

namespace EA.Net
{

    [Serializable]
    public class EAConfig
    {
        private int mNumberObjectives;
        private int mNumberVarsReal;
        private int mNumberVarsBin;
        private bool mFlagAutoPopSize = false;
        private int mMaxGen = 200;
        private int mPopSize = 100;
        private Vars.eCoefsVariation mCoefsVariation = Vars.eCoefsVariation.Random;
        private bool mFlagUseMutation = true;
        private bool mFlagUseElitism = false;
        private double mProbMutation = 0.05;
        private bool mFlagAutoProbMutation = true;

        /* NSGA */
        private double mEPS = 1e-14;
        private double mProbCrossover = 0.90;
        private double mIndexDistribuitionCross = 15;
        private double mIndexDistribuitionMut = 20;

        /* MoDE */
        private double mCR1 = 0.45;
        private double mF1 = 0.35;
        private double mCR2 = 0.5;
        private double mF2 = 0.6;
        private Vars.eEstrategiaDE mStrategy = Vars.eEstrategiaDE.DE_current_to_rand_1_bin;

        /* OMOPSO */
        private double mC1Ini = 0.3;
        private double mC2Ini = 0.3;
        private double mWIni = 0.1;
        private double mC1Fim = 0.6;
        private double mC2Fim = 0.6;
        private double mWFim = 0.6;

        private bool mAlterado;

        [NonSerialized]
        private BackgroundWorker _bckProcess;

        [XmlIgnore]
        public BackgroundWorker bckProcess
        {
            set { _bckProcess = value; }
            get { return _bckProcess; }
        }

        #region /* Geral */

        public Vars.eCoefsVariation CoefsVariation
        {
            get { return mCoefsVariation; }
            set { mCoefsVariation = value; }
        }

        public bool FlagAutoPopSize
        {
            get { return mFlagAutoPopSize; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagAutoPopSize); }
        }

        public bool FlagAutoProbMutation
        {
            get { return mFlagAutoProbMutation; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagAutoProbMutation); }
        }


        public bool FlagUseMutation
        {
            get { return mFlagUseMutation; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagUseMutation); }
        }

        public bool FlagUseElitism
        {
            get { return mFlagUseElitism; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagUseElitism); }
        }

        public int NumberVarsReal
        {
            get { return mNumberVarsReal; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mNumberVarsReal); }
        }

        public int NumberVarsBin
        {
            get { return mNumberVarsBin; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mNumberVarsBin); }
        }

        public int NumberObjectives
        {
            get { return mNumberObjectives; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mNumberObjectives); }
        }

        public int PopSize
        {
            get { return mPopSize; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mPopSize); }
        }

        public int MaxGEN
        {
            get { return mMaxGen; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mMaxGen); }
        }

        public double ProbMutation
        {
            get { return mProbMutation; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mProbMutation); }
        }
        #endregion

        #region /* MoDE */
        public Vars.eEstrategiaDE Strategy
        {
            get { return mStrategy; }
            set { mStrategy = value; }
        }

        public double CR1
        {
            get { return mCR1; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mCR1); }
        }

        public double F1
        {
            get { return mF1; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mF1); }
        }

        public double CR2
        {
            get { return mCR2; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mCR2); }
        }

        public double F2
        {
            get { return mF2; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mF2); }
        }
        #endregion

        #region /* NSGA II */
        public double EPS
        {
            get { return mEPS; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mEPS); }
        }

        public double ProbCrossover
        {
            get { return mProbCrossover; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mProbCrossover); }
        }

        public double IndexDistribuitionCross
        {
            get { return mIndexDistribuitionCross; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mIndexDistribuitionCross); }
        }

        public double IndexDistribuitionMut
        {
            get { return mIndexDistribuitionMut; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mIndexDistribuitionMut); }
        }
        #endregion

        #region /* MOPSO */
        public double C1Ini
        {
            get { return mC1Ini; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mC1Ini); }
        }

        public double C1Fim
        {
            get { return mC1Fim; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mC1Fim); }
        }

        public double C2Ini
        {
            get { return mC2Ini; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mC2Ini); }
        }

        public double C2Fim
        {
            get { return mC2Fim; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mC2Fim); }
        }

        public double WIni
        {
            get { return mWIni; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mWIni); }
        }

        public double WFim
        {
            get { return mWFim; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mWFim); }
        }
        #endregion

         [NonSerialized]
        private delegateFO _CalculaFO;
        
        public delegate void delegateFO(ref byte[] Xbin, ref double[] Xreal, ref double[] Objective, ref double Constraint);

         [XmlIgnore]
        public delegateFO CalculaFO
        {
            get
            {
                return _CalculaFO;
            }
            set
            {
                _CalculaFO = value;
            }
        }
    }
}
