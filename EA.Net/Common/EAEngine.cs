﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;


namespace EA.Net
{
    public partial class EAEngine : Util
    {
        public double[] XUpperReal, XLowerReal;
        public double[] XUpperBin, XLowerBin;
        public long ProcessingTime;
        public Population ParentPopulation;
        public Population ChildPopulation;
        public Population MixedPopulation;

        public EAEngine()
        {
            EAConfig = new EAConfig();
        }
        
        public void Optimize(Vars.eAlgorithm Algorithm)
        {
            Algorithm algo;
            switch (Algorithm)
            {
                case Vars.eAlgorithm.MoDE_NS:
                    algo = new MoDE();
                    algo.EA = this;
                    algo.Optimize();
                    break;
                case Vars.eAlgorithm.NSGAII:
                    algo = new NSGAII();
                    algo.EA = this;
                    algo.Optimize();
                    break;
                case Vars.eAlgorithm.MoPSO_NS:
                    algo = new MoPSO_NS();
                    algo.EA = this;
                    algo.Optimize();
                    break;
            }
        }

        /* Function to initialize an individual randomly */
        public void InitializePopulation()
        {
            ParentPopulation = new Population();
            ChildPopulation = new Population();
            MixedPopulation = new Population();

            ParentPopulation.Ind = new Individual[EAConfig.PopSize];
            ChildPopulation.Ind = new Individual[EAConfig.PopSize];
            MixedPopulation.Ind = new Individual[2 * (EAConfig.PopSize)];

            for (int i = 0, k = EAConfig.PopSize; i < EAConfig.PopSize; i++, k++)
            {
                InitializeRnd(ref ParentPopulation.Ind[i]);
                InitializeRnd(ref ChildPopulation.Ind[i]);

                InitializeRnd(ref MixedPopulation.Ind[i]);
                InitializeRnd(ref MixedPopulation.Ind[k]);
            }
        }

        /* Function to initialize an individual randomly */
        private void InitializeRnd(ref Individual Ind)
        {
            Ind = new Individual();

            Ind.Xreal = new double[EAConfig.NumberVarsReal];
            Ind.Xbin = new byte[EAConfig.NumberVarsBin];

            Ind.Objective = new double[EAConfig.NumberObjectives];

            for (int j = 0; j < Ind.Xreal.Count(); j++)
                Ind.Xreal[j] = RND.rnd(XLowerReal[j], XUpperReal[j]);
            
            for (int j = 0; j < Ind.Xbin.Count() - 1; j++)
            {
                if (RND.rndDouble() > 0.5)
                    Ind.Xbin[j] = 1;
                else
                    Ind.Xbin[j] = 0;
            }
        }
    }
}
