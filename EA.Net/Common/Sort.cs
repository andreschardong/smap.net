﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class Util
    {

        // Randomized quick sort routine to sort a population based on a particular objective chosen 
        public void QuickSortFrontObj(ref Population Pop, int ObjCount, ref int[] ObjArray, int ObjArraySize)
        {
            QSortFrontObj(Pop, ObjCount, ref ObjArray, 0, ObjArraySize - 1);
        }

        // Actual implementation of the randomized quick sort used to sort a population based on a particular objective chosen 
        public void QSortFrontObj(Population Pop, int ObjCount, ref int[] ObjArray, int Left, int Right)
        {
            int Index, Temp, i;
            double pivot;

            if (Left < Right)
            {
                Index = RND.rndint(Left, Right);
                Temp = ObjArray[Right];
                ObjArray[Right] = ObjArray[Index];
                ObjArray[Index] = Temp;
                pivot = Pop.Ind[ObjArray[Right]].Objective[ObjCount];
                i = Left - 1;
                for (int j = Left; j < Right; j++)
                {
                    if (Pop.Ind[ObjArray[j]].Objective[ObjCount] <= pivot)
                    {
                        i++;
                        Temp = ObjArray[j];
                        ObjArray[j] = ObjArray[i];
                        ObjArray[i] = Temp;
                    }
                }
                Index = i + 1;

                Temp = ObjArray[Index];
                ObjArray[Index] = ObjArray[Right];
                ObjArray[Right] = Temp;

                QSortFrontObj(Pop, ObjCount, ref ObjArray, Left, Index - 1);
                QSortFrontObj(Pop, ObjCount, ref ObjArray, Index + 1, Right);
            }
        }

        // Randomized quick sort routine to sort a population based on crowding distance 
        public void QuickSortDist(Population pop, int[] dist, int FrontSize)
        {
            QSortDistance(pop, ref dist, 0, FrontSize - 1);
        }

        // Actual implementation of the randomized quick sort used to sort a population based on crowding distance 
        private void QSortDistance(Population Pop, ref int[] Dist, int Left, int Right)
        {
            int Index, Temp, i;
            double pivot;
            if (Left < Right)
            {
                Index = RND.rndint(Left, Right);
                Temp = Dist[Right];
                Dist[Right] = Dist[Index];
                Dist[Index] = Temp;
                pivot = Pop.Ind[Dist[Right]].CrowdingDistance;
                i = Left - 1;

                for (int j = Left; j < Right; j++)
                {
                    if (Pop.Ind[Dist[j]].CrowdingDistance <= pivot)
                    {
                        i++;
                        Temp = Dist[j];
                        Dist[j] = Dist[i];
                        Dist[i] = Temp;
                    }
                }
                Index = i + 1;
                Temp = Dist[Index];
                Dist[Index] = Dist[Right];
                Dist[Right] = Temp;

                QSortDistance(Pop, ref Dist, Left, Index - 1);
                QSortDistance(Pop, ref Dist, Index + 1, Right);
            }
        }
    }
}