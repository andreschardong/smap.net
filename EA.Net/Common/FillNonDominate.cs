﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EA.Net
{
    public partial class Util
    {
        private ListEAUtil lstUtil = new ListEAUtil();

        /* Routine to perform non-dominated sorting */
        public void FillNondominatedSort(Population MixedPop, ref Population NewPop)
        {
            int Flag, i, j, End, Rank = 1;
            int FrontSize = 0;
            int ArchieveSize = 0;

            NodeEA Pool = new NodeEA();
            NodeEA Elite = new NodeEA();
            NodeEA Temp1, Temp2;

            Pool.Index = -1;
            Pool.Parent = null;
            Pool.Child = null;
            Elite.Index = -1;
            Elite.Parent = null;
            Elite.Child = null;
            Temp1 = Pool;

            for (i = 0; i < 2 * _EAConfig.PopSize; i++)
            {
                lstUtil.Insert(ref Temp1, i);
                Temp1 = Temp1.Child;
            }
            i = 0;

            do
            {
                Temp1 = Pool.Child;
                lstUtil.Insert(ref Elite, Temp1.Index);
                FrontSize = 1;
                Temp2 = Elite.Child;
                Temp1 = lstUtil.Delete(Temp1);
                Temp1 = Temp1.Child;

                do
                {
                    Temp2 = Elite.Child;
                    if (Temp1 == null)
                        break;
                    do
                    {
                        End = 0;
                        Flag = ChekDominace(MixedPop.Ind[Temp1.Index], MixedPop.Ind[Temp2.Index]);

                        if (Flag == 1)
                        {
                            lstUtil.Insert(ref Pool, Temp2.Index);
                            Temp2 = lstUtil.Delete(Temp2);
                            FrontSize--;
                            Temp2 = Temp2.Child;
                        }

                        else if (Flag == 0)
                            Temp2 = Temp2.Child;

                        else if (Flag == -1)
                            End = 1;
                    }
                    while (End != 1 && Temp2 != null);

                    if (Flag == 0 || Flag == 1)
                    {
                        lstUtil.Insert(ref Elite, Temp1.Index);
                        FrontSize++;
                        Temp1 = lstUtil.Delete(Temp1);
                    }
                    Temp1 = Temp1.Child;
                }
                while (Temp1 != null);

                Temp2 = Elite.Child;
                j = i;
                if (ArchieveSize + FrontSize <= _EAConfig.PopSize)
                {
                    do
                    {
                        CopyIndividual(MixedPop.Ind[Temp2.Index], ref NewPop.Ind[i]);
                        NewPop.Ind[i].Rank = Rank;
                        ArchieveSize++;
                        Temp2 = Temp2.Child;
                        i ++;
                    }
                    while (Temp2 != null);
                    AssignCrowdingDistanceIndices(ref NewPop, j, i - 1);
                    Rank ++;
                }
                else
                {
                    CrowdingFill(MixedPop, NewPop, i, FrontSize, Elite);
                    ArchieveSize = _EAConfig.PopSize;
                    for (j = i; j < _EAConfig.PopSize; j++)
                    {
                        NewPop.Ind[j].Rank = Rank;
                    }
                }
                Temp2 = Elite.Child;

                do
                {
                    Temp2 = lstUtil.Delete(Temp2);
                    Temp2 = Temp2.Child;
                }
                while (Elite.Child != null);
            }
            while (ArchieveSize < _EAConfig.PopSize);

            //while (Pool != null)
            //{
            //    Temp1 = Pool;
            //    Pool = Pool.Child;
            //}
            //while (Elite != null)
            //{
            //    Temp1 = Elite;
            //    Elite = Elite.Child;
            //}
        }

        /* Routine to fill a population with Individuals in the decreasing order of crowding distance */
        private void CrowdingFill(Population MixedPop, Population NewPop, int count, int FrontSize, NodeEA Elite)
        {
            int[] Dist;
            NodeEA Temp;
            int i, j;

            AssignCrowdingDistanceList(ref MixedPop, Elite.Child, FrontSize);

            Dist = new int[FrontSize];

            Temp = Elite.Child;
            for (j = 0; j < FrontSize; j++)
            {
                Dist[j] = Temp.Index;
                Temp = Temp.Child;
            }

            QuickSortDist(MixedPop, Dist, FrontSize);

            for (i = count, j = FrontSize - 1; i < _EAConfig.PopSize; i++, j--)
            {
                CopyIndividual(MixedPop.Ind[Dist[j]], ref NewPop.Ind[i]);
            }
        }
    }
}