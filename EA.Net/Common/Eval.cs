﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class Util
    {
        public void EvaluatePopulation(ref Population pop)
        {
            for (int i = 0; i < EAConfig.PopSize; i++)
                EAConfig.CalculaFO(ref  pop.Ind[i].Xbin, ref pop.Ind[i].Xreal, ref pop.Ind[i].Objective, ref pop.Ind[i].ConstraintViolation);
        }

    }
}