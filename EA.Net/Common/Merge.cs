﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class Util
    {
        /* Routine to merge two populations into one */
        public void Merge(Population Pop1, Population Pop2, ref Population Pop3)
        {
            for (int i = 0; i < _EAConfig.PopSize; i++)
                CopyIndividual(Pop1.Ind[i], ref Pop3.Ind[i]);
            
            for (int i = 0, k = _EAConfig.PopSize; i < _EAConfig.PopSize; i++, k++)
                CopyIndividual(Pop2.Ind[i], ref Pop3.Ind[k]);
        }

        /* Routine to copy an individual 'ind1' into another individual 'ind2' */
        public void CopyIndividual(Individual Ind1, ref Individual Ind2)
        {
            Ind2.Rank = Ind1.Rank;
            Ind2.ConstraintViolation = Ind1.ConstraintViolation;
            Ind2.CrowdingDistance = Ind1.CrowdingDistance;
            
            for (int i = 0; i < _EAConfig.NumberVarsReal; i++)
                Ind2.Xreal[i] = Ind1.Xreal[i];

            for (int i = 0; i < _EAConfig.NumberVarsBin; i++)
                Ind2.Xbin[i] = Ind1.Xbin[i];

            for (int i = 0; i < Ind2.Objective.Count(); i++)
                Ind2.Objective[i] = Ind1.Objective[i];
        }
    }
}