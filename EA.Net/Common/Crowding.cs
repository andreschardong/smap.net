﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EA.Net
{
    public partial class Util
    {
        /* Routine to compute crowding distance based on ojbective function values when the population in in the form of a lst */
        public void AssignCrowdingDistanceList(ref Population Pop, NodeEA List, int FrontSize)
        {
            int[][] ObjectArray;
            int[] Dist;
            NodeEA Temp;
            int i, j;

            Temp = List;
            if (FrontSize == 1)
            {
                Pop.Ind[List.Index].CrowdingDistance = double.PositiveInfinity;
                return;
            }
            if (FrontSize == 2)
            {
                Pop.Ind[List.Index].CrowdingDistance = double.PositiveInfinity;
                Pop.Ind[List.Child.Index].CrowdingDistance = double.PositiveInfinity;
                return;
            }

            ObjectArray = new int[_EAConfig.NumberObjectives][];
            for (i = 0; i < _EAConfig.NumberObjectives; i++)
                ObjectArray[i] = new int[FrontSize];

            Dist = new int[FrontSize];
            for (j = 0; j < FrontSize; j++)
            {
                Dist[j] = Temp.Index;
                Temp = Temp.Child;
            }

            AssignCrowdingDistance(ref Pop, Dist, ObjectArray, FrontSize);
        }

        /* Routine to compute crowding distance based on objective function values when the population in in the form of an array */
        public void AssignCrowdingDistanceIndices(ref Population Pop, int c1, int c2)
        {
            int[][] ObjArray;
            int[] Dist;

            int FrontSize = c2 - c1 + 1;

            if (FrontSize == 1)
            {
                Pop.Ind[c1].CrowdingDistance = double.PositiveInfinity;
                return;
            }
            if (FrontSize == 2)
            {
                Pop.Ind[c1].CrowdingDistance = double.PositiveInfinity;
                Pop.Ind[c2].CrowdingDistance = double.PositiveInfinity;
                return;
            }

            ObjArray = new int[_EAConfig.NumberObjectives][];

            for (int i = 0; i < _EAConfig.NumberObjectives; i++)
                ObjArray[i] = new int[FrontSize];

            Dist = new int[FrontSize];

            for (int j = 0; j < FrontSize; j++)
                Dist[j] = c1++;

            AssignCrowdingDistance(ref Pop, Dist, ObjArray, FrontSize);
        }

        /* Routine to compute crowding distances */
        public void AssignCrowdingDistance(ref Population pop, int[] dist, int[][] obj_array, int FrontSize)
        {
            int i, j;
            for (i = 0; i < _EAConfig.NumberObjectives; i++)
            {
                for (j = 0; j < FrontSize; j++)
                    obj_array[i][j] = dist[j];

                QuickSortFrontObj(ref pop, i, ref obj_array[i], FrontSize);
            }
            for (j = 0; j < FrontSize; j++)
                pop.Ind[dist[j]].CrowdingDistance = 0.0;

            for (i = 0; i < _EAConfig.NumberObjectives; i++)
                pop.Ind[obj_array[i][0]].CrowdingDistance = double.PositiveInfinity;

            for (i = 0; i < _EAConfig.NumberObjectives; i++)
            {
                for (j = 1; j < FrontSize - 1; j++)
                {
                    if (pop.Ind[obj_array[i][j]].CrowdingDistance != double.PositiveInfinity)
                    {
                        if (pop.Ind[obj_array[i][FrontSize - 1]].Objective[i] == pop.Ind[obj_array[i][0]].Objective[i])
                            pop.Ind[obj_array[i][j]].CrowdingDistance += 0.0;

                        else
                            pop.Ind[obj_array[i][j]].CrowdingDistance += (pop.Ind[obj_array[i][j + 1]].Objective[i] - pop.Ind[obj_array[i][j - 1]].Objective[i]) / (pop.Ind[obj_array[i][FrontSize - 1]].Objective[i] - pop.Ind[obj_array[i][0]].Objective[i]);
                    }
                }
            }
            for (j = 0; j < FrontSize; j++)
            {
                if (pop.Ind[dist[j]].CrowdingDistance != double.PositiveInfinity)
                    pop.Ind[dist[j]].CrowdingDistance = (pop.Ind[dist[j]].CrowdingDistance) / _EAConfig.NumberObjectives;
            }
        }
    }
}