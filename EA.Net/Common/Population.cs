﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Xml.Serialization;

namespace EA.Net
{
 
    [Serializable]
    public class Population
    {
        public Individual[] Ind;
        public Population TrueFront;
        public double[] XComp;
        public double[] YComp;
        public double GD, iGD, Spacing, Spread, DD;
        public long ProcessingTime;
    }
    

}
