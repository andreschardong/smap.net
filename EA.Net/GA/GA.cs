﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace EA.Net
{
    [Serializable()]
    public class GAConfig
    {

        private bool mFlagAutoPopSize = true;
        private double mProbCrossover = 0.95;
        private double mProbMutation = 0.05;
        private bool mFlagRigid = true;
        private bool mFlagSharing = true;
        private double mSharingDistance = 0.4;
        private double mIndexDistribuitionCross = 5;
        private double mIndexDistribuitionMut = 20;
        private double mSeedRnd = 0.5;
        private int mMaxGen = 200;
        private int mPopSize = 200;
        private int mNumberVars = 0;
        private int mNumberConst = 0;
        private bool mAlterado;

        public bool FlagAutoPopSize
        {
            get { return mFlagAutoPopSize; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagAutoPopSize); }
        }

        public int PopSize
        {
            get { return mPopSize; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mPopSize); }
        }

        public int NumberVars
        {
            get { return mNumberVars; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mNumberVars); }
        }

        public int NumberConst
        {
            get { return mNumberConst; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mNumberConst); }
        }

        public int MaxGEN
        {
            get { return mMaxGen; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mMaxGen); }
        }

        public double ProbCrossover
        {
            get { return mProbCrossover; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mProbCrossover); }
        }

        public double ProbMutation
        {
            get { return mProbMutation; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mProbMutation); }
        }

        public bool FlagRigid
        {
            get { return mFlagRigid; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagRigid); }
        }

        public bool FlagSharing
        {
            get { return mFlagSharing; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mFlagSharing); }
        }

        public double SharingDistance
        {
            get { return mSharingDistance; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mSharingDistance); }
        }

        public double IndexDistribuitionCross
        {
            get { return mIndexDistribuitionCross; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mIndexDistribuitionCross); }
        }

        public double IndexDistribuitionMut
        {
            get { return mIndexDistribuitionMut; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mIndexDistribuitionMut); }
        }

        public double SeedRnd
        {
            get { return mSeedRnd; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref  mSeedRnd); }
        }

        private delegateFO _CalculaFO;
        public delegate double delegateFO(double[] VetorX, ref double penalty);

        [System.Xml.Serialization.XmlIgnore]
        public delegateFO CalculaFO
        {
            get
            {
                return _CalculaFO;
            }
            set
            {
                _CalculaFO = value;
            }
        }
    }

    [Serializable()]
    public class Genoma
    {

        public double[] X;
        public int CrossVar;
        public double Objective;
        public double Penalty;
        public int Parent1;
        public int Parent2;

    }

    public class GA
    {

        private GAConfig _GAConfig;
        public GAConfig GAConfig
        {
            set { _GAConfig = value; }
            get { return _GAConfig; }
        }

        private List<double> _FOList = new List<double>();

        public List<double> FOlist
        {
            set { _FOList = value; }
            get { return _FOList; }
        }

        private int NumVars;
        private int NumConst;
        private int MaxPopSize;
        private int MaxGen;

        private bool FlagSharing;
        private bool FlagRigid;

        private double ProbMutation;
        private double ProbCrossover;
        private double n_distribution_m;
        private double n_distribution_c;

        private double Infinity = double.MaxValue;
        private double Epsilon = 1E-06;

        private double SharingDistance;
        private double SeedRnd = 0.1;

        public double[] XUpper;
        public double[] XLower;

        private int NCrossover;

        private int[] TourneyList;
        private int TourneyPos;
        private int TourneySize = 2;
        private int CriticalSize;

        private double SumObjective;
        private double AvgObjective;
        private double MaxObjective;
        private double MinObjective;
        //, MinX(), MaxX() As Double

        public int BestEverGerId;

        private List<Genoma> OldPopulation;
        private List<Genoma> NewPopulation;
        public Genoma BestEver;
        private Genoma CurrentBest;

        //Private oldrand(54) As Double
        //Private jrand As Integer = 0
        private Random rnd1 = new Random();

        public GA()
        {
            
        }
        
        public void Optimize()
        {
            BackgroundWorker bckProcess;
            bckProcess = null;

            Optimize(ref bckProcess);
        }

        public void Optimize(ref BackgroundWorker bckProcess)
        {

            //IniciaRnd(SeedRnd)
            FlagRigid = _GAConfig.FlagRigid;
            FlagSharing = _GAConfig.FlagSharing;
            ProbCrossover = _GAConfig.ProbCrossover;
            ProbMutation = _GAConfig.ProbMutation;
            n_distribution_c = _GAConfig.IndexDistribuitionCross;
            n_distribution_m = _GAConfig.IndexDistribuitionMut;

            NumVars = _GAConfig.NumberVars;
            NumConst = _GAConfig.NumberConst;
            MaxPopSize = _GAConfig.PopSize;
            MaxGen = _GAConfig.MaxGEN;

            BestEver = new Genoma();
            CurrentBest = new Genoma();

            OldPopulation = new List<Genoma>();
            NewPopulation = new List<Genoma>();

            TourneyList = new int[MaxPopSize];

            System.Collections.Generic.List<double> FOList = new List<double>();

            double u = 0;
            for (int k = 0; k <= MaxPopSize - 1; k++)
            {
                OldPopulation.Add(new Genoma());
                NewPopulation.Add(new Genoma());

                OldPopulation[k].X = new double[NumVars];
                NewPopulation[k].X = new double[NumVars];
                for (int j = 0; j <= NumVars - 1; j++)
                {
                    u = randomperc();
                    OldPopulation[k].X[j] = XLower[j] + (XUpper[j] - XLower[j]) * (1 - u);
                }
            }
            NCrossover = 0;

            BestEver = Clone(OldPopulation[0]);
            CalcObjective(BestEver);

            int GenNumber = 0;

            Evaluate(GenNumber);
            List<Genoma> temp = default(List<Genoma>);

            for (GenNumber = 1; GenNumber <= MaxGen; GenNumber++)
            {

                GenerateNewPopulation();

                temp = OldPopulation;
                OldPopulation = NewPopulation;
                NewPopulation = temp;

                Evaluate(GenNumber);

                FOList.Add(BestEver.Objective);

                if (bckProcess != null)
                    bckProcess.ReportProgress((int)((float)GenNumber / MaxGen * 100), GenNumber);
            }

            double p = 0;
            _GAConfig.CalculaFO(BestEver.X, ref p);
        }

        private void GenerateNewPopulation()
        {

            int mate1 = 0;
            int mate2 = 0;

            PreSelectTour();

            for (int k = 0; k <= MaxPopSize - 1; k += 2)
            {

                //selection
                if (FlagSharing)
                {
                    mate1 = TourSelectConst();
                    mate2 = TourSelectConst();
                }
                else
                {
                    mate1 = TourSelect();
                    mate2 = TourSelect();
                }

                //crossover
                Crossover(mate1, mate2, k, k + 1);

                //mutation
                Mutation(NewPopulation[k]);
                Mutation(NewPopulation[k + 1]);

                NewPopulation[k + 1].Parent1 = mate1 + 1;
                NewPopulation[k + 1].Parent2 = mate2 + 1;
                NewPopulation[k].Parent1 = mate1 + 1;
                NewPopulation[k].Parent2 = mate2 + 1;
            }
        }

        private void PreSelectTour()
        {

            int rand1 = 0;
            int rand2 = 0;
            int temp_site = 0;

            for (int i = 0; i <= MaxPopSize - 1; i++)
            {
                TourneyList[i] = i;
            }

            for (int i = 0; i <= MaxPopSize - 1; i++)
            {

                rand1 = rnd(0, MaxPopSize - 1);
                rand2 = rnd(0, MaxPopSize - 1);
                temp_site = TourneyList[rand1];
                TourneyList[rand1] = TourneyList[rand2];
                TourneyList[rand2] = temp_site;
            }

            TourneyPos = 0;
        }

        private int TourSelect()
        {

            int pick = 0;
            int winner = 0;
        start_select:

            if ((MaxPopSize - TourneyPos) < TourneySize)
            {
                PreSelectTour();
            }

            winner = TourneyList[TourneyPos];

            if ((winner < 0 | winner > MaxPopSize - 1))
            {

                PreSelectTour();
                goto start_select;
            }

            for (int i = 1; i <= TourneySize - 1; i++)
            {

                pick = TourneyList[i + TourneyPos];

                if (pick < 0 | pick > MaxPopSize - 1)
                {
                    PreSelectTour();
                    goto start_select;
                }

                if (OldPopulation[winner].Penalty > OldPopulation[pick].Penalty)
                {
                    winner = pick;
                }
                else if (OldPopulation[winner].Penalty <= 0.0 && OldPopulation[pick].Penalty <= 0.0)
                {
                    if (OldPopulation[pick].Objective < OldPopulation[winner].Objective) winner = pick;
                }
            }

            TourneyPos += TourneySize;

            return winner;
        }

        private double Distance(int one, int two)
        {

            double sum = 0;

            sum = 0.0;
            for (int k = 0; k <= NumVars - 1; k++)
            {
                sum += Math.Pow((OldPopulation[one].X[k] - OldPopulation[two].X[k]) / (XUpper[k] - XLower[k]), 2);
            }

            return Math.Sqrt(sum / NumVars);
        }

        private int TourSelectConst()
        {

            int pick = 0;
            int winner = 0;
            int minus = 0;
            int rand_pick = 0;
            int rand_indv = 0;
            int flag = 0;
        start_select:

            if ((MaxPopSize - TourneyPos) < TourneySize)
            {
                PreSelectTour();
            }

            winner = TourneyList[TourneyPos];

            if (winner < 0 | winner > MaxPopSize - 1)
            {
                PreSelectTour();
                goto start_select;
            }

            for (int i = 1; i <= TourneySize - 1; i++)
            {

                pick = TourneyList[i + TourneyPos];

                if (OldPopulation[winner].Penalty > 0.0 && OldPopulation[pick].Penalty <= 0.0)
                {
                    winner = pick;
                }
                else if (((OldPopulation[winner].Penalty > 0.0) && (OldPopulation[pick].Penalty > 0.0)))
                {
                    if (OldPopulation[pick].Penalty < OldPopulation[winner].Penalty) winner = pick;
                }
                else if (OldPopulation[winner].Penalty <= 0.0 && OldPopulation[pick].Penalty <= 0.0)
                {

                    if (Distance(winner, pick) < SharingDistance)
                    {
                        if (OldPopulation[pick].Objective < OldPopulation[winner].Objective) winner = pick;
                    }
                    else
                    {
                        minus = -1;
                        flag = 0;
                        for (int indv = 0; indv <= CriticalSize - 1; indv++)
                        {

                            rand_indv = rnd(0, MaxPopSize - 1);
                            rand_pick = TourneyList[rand_indv];
                            if (OldPopulation[rand_pick].Penalty <= 0.0)
                            {

                                if (Distance(winner, rand_pick) < SharingDistance)
                                {
                                    flag = 1;
                                    if (OldPopulation[rand_pick].Objective < OldPopulation[winner].Objective) winner = rand_pick;
                                }
                            }
                            if (flag == 1) break; // TODO: might not be correct. Was : Exit For

                        }
                    }
                }

                if (pick < 0 | pick > MaxPopSize - 1)
                {
                    PreSelectTour();
                    goto start_select;
                }
            }

            TourneyPos += TourneySize + minus;

            return winner;
        }

        private void Crossover(int first, int second, int childno1, int childno2)
        {

            if (Flip(ProbCrossover))
            {

                NCrossover += 1;
                for (int site = 0; site <= NumVars - 1; site++)
                {

                    if (Flip(0.5) | NumVars == 1)
                    {
                        CreateChildren(OldPopulation[first].X[site], OldPopulation[second].X[site], ref NewPopulation[childno1].X[site], ref NewPopulation[childno2].X[site], XLower[site], XUpper[site]);
                    }
                    else
                    {
                        NewPopulation[childno1].X[site] = OldPopulation[first].X[site];
                        NewPopulation[childno2].X[site] = OldPopulation[second].X[site];
                    }
                }
                NewPopulation[childno1].CrossVar = 0;
                NewPopulation[childno2].CrossVar = 0;
            }
            else
            {
                for (int site = 0; site <= NumVars - 1; site++)
                {
                    NewPopulation[childno1].X[site] = OldPopulation[first].X[site];
                    NewPopulation[childno2].X[site] = OldPopulation[second].X[site];
                }
                NewPopulation[childno1].CrossVar = 0;
                NewPopulation[childno2].CrossVar = 0;
            }
        }

        private void CreateChildren(double p1, double p2, ref double c1, ref double c2, double low, double high)
        {

            double difference = 0;
            double x_mean = 0;
            double beta = 0;
            double v2 = 0;
            double v1 = 0;
            double u = 0;
            double dist = 0;
            double umax = 0;
            double temp = 0;
            double alpha = 0;

            if ((p1 > p2))
            {
                temp = p1;
                p1 = p2;
                p2 = temp;
            }

            x_mean = (p1 + p2) * 0.5;
            difference = p2 - p1;
            if ((p1 - low) < (high - p2))
            {
                dist = p1 - low;
            }
            else
            {
                dist = high - p2;
            }
            if (dist < 0.0) dist = 0.0;
            if (FlagRigid && difference > Epsilon)
            {

                alpha = 1.0 + (2.0 * dist / difference);
                umax = 1.0 - (0.5 / Math.Pow(alpha, n_distribution_c + 1.0));

                u = umax * randomperc();
            }
            else
            {
                u = randomperc();
            }
            beta = GetBeta(u);
            if (Math.Abs(difference * beta) > Infinity) beta = double.PositiveInfinity / difference;
            v2 = x_mean + beta * 0.5 * difference;
            v1 = x_mean - beta * 0.5 * difference;

            if (v2 < low) v2 = low;
            if (v2 > high) v2 = high;
            if (v1 < low) v2 = low;
            if (v1 > high) v2 = high;
            c2 = v2;


            c1 = v1;
        }

        private void Mutation(Genoma indiv)
        {

            double delta_l = 0;
            double delta_u = 0;
            double delta = 0;

            for (int site = 0; site <= NumVars - 1; site++)
            {

                if (Flip(ProbMutation))
                {
                    if (FlagRigid)
                    {
                        delta_l = (XLower[site] - indiv.X[site]) / (XUpper[site] - XLower[site]);
                        if (delta_l < -1.0) delta_l = -1.0;

                        delta_u = (XUpper[site] - indiv.X[site]) / (XUpper[site] - XLower[site]);
                        if (delta_u > 1.0) delta_u = 1.0;

                        if (-1.0 * delta_l < delta_u)
                            delta_u = -1.0 * delta_l;
                        else
                            delta_l = -1.0 * delta_u;
                    }
                    else
                    {
                        delta_l = -1.0;
                        delta_u = 1.0;
                    }

                    delta = GetDelta(randomperc(), delta_l, delta_u) * (XUpper[site] - XLower[site]);
                    indiv.X[site] += delta;

                    if (indiv.X[site] < XLower[site]) indiv.X[site] = XLower[site];
                    if (indiv.X[site] > XUpper[site]) indiv.X[site] = XUpper[site];
                }
            }
        }


        private double GetDelta(double u, double delta_l, double delta_u)
        {

            double delta = 0;
            double aa = 0;

            if (u >= 1.0 - 1E-09)
            {
                delta = delta_u;
            }
            else if (u <= 0.0 + 1E-09)
            {
                delta = delta_l;
            }
            else
            {
                if (u <= 0.5)
                {
                    aa = 2.0 * u + (1.0 - 2.0 * u) * Math.Pow(1 + delta_l, n_distribution_m + 1.0);
                    delta = Math.Pow(aa, 1.0 / (n_distribution_m + 1.0)) - 1.0;
                }
                else
                {
                    aa = 2.0 * (1 - u) + 2.0 * (u - 0.5) * Math.Pow(1 - delta_u, n_distribution_m + 1.0);
                    delta = 1.0 - Math.Pow(aa, 1.0 / (n_distribution_m + 1.0));
                }
            }
            if (delta < -1.0 | delta > 1.0)
            {
                throw new Exception("Error in mutation!! delta =  " + delta.ToString());
            }

            return delta;
        }

        private double GetBeta(double u)
        {

            double beta = 0;

            if (1.0 - u < Epsilon) u = 1.0 - Epsilon;
            if (u < 0.0) u = 0.0;
            if (u < 0.5)
            {
                beta = Math.Pow(2.0 * u, 1.0 / (n_distribution_c + 1.0));
            }
            else
            {
                beta = Math.Pow(0.5 / (1.0 - u), 1.0 / (n_distribution_c + 1.0));
            }

            return beta;
        }

        private bool Flip(double prob)
        {

            if (randomperc() <= prob)
                return true;
            else
                return false;
        }

        private void Evaluate(int gen)
        {

            int change_flag = 0;

            for (int k = 0; k <= MaxPopSize - 1; k++)
            {
                CalcObjective(OldPopulation[k]);
            }

            CurrentBest = Clone(OldPopulation[0]);
            SumObjective = OldPopulation[0].Objective;
            AvgObjective = OldPopulation[0].Objective;
            MaxObjective = OldPopulation[0].Objective;
            MinObjective = OldPopulation[0].Objective;

            for (int k = 1; k <= MaxPopSize - 1; k++)
            {

                if (CurrentBest.Penalty > OldPopulation[k].Penalty)
                {
                    CurrentBest = Clone(OldPopulation[k]);
                }
                else if (CurrentBest.Penalty <= 0.0 && OldPopulation[k].Penalty <= 0.0)
                {
                    if (CurrentBest.Objective > OldPopulation[k].Objective) CurrentBest = Clone(OldPopulation[k]);
                    if (MaxObjective < OldPopulation[k].Objective) MaxObjective = OldPopulation[k].Objective;
                    if (MinObjective > OldPopulation[k].Objective) MinObjective = OldPopulation[k].Objective;
                }
                SumObjective += OldPopulation[k].Objective;
            }

            AvgObjective = SumObjective / MaxPopSize;
            change_flag = 0;

            if (BestEver.Penalty > CurrentBest.Penalty)
                change_flag = 1;
            else if (BestEver.Penalty <= 0.0 && CurrentBest.Penalty <= 0)
            {
                if (BestEver.Objective > CurrentBest.Objective) 
                    change_flag = 1;
            }

            if (change_flag == 1)
            {
                BestEver = Clone(CurrentBest);
                BestEverGerId = gen;
            }
        }

        private void CalcObjective(Genoma genoma)
        {

            genoma.Penalty = 0;
            genoma.Objective = _GAConfig.CalculaFO(genoma.X, ref genoma.Penalty);
        }

        private Genoma Clone(Genoma origem)
        {
            MemoryStream m = new MemoryStream();
            BinaryFormatter b = new BinaryFormatter();
            b.Serialize(m, origem);
            m.Seek(0, SeekOrigin.Begin);
            return (Genoma)b.Deserialize(m);
        }


        //Private Sub IniciaRnd(ByVal SeedRn As Double)

        //    Dim j1, ii As Integer

        //    Dim new_random, prev_random As Double

        //    oldrand(54) = SeedRn
        //    new_random = 0.000000001
        //    prev_random = SeedRn
        //    For j1 = 1 To 54

        //        ii = (21 * j1) Mod 54
        //        oldrand(ii) = new_random
        //        new_random = prev_random - new_random
        //        If (new_random < 0.0) Then new_random = new_random + 1.0
        //        prev_random = oldrand(ii)
        //    Next

        //    advance_random()
        //    advance_random()
        //    advance_random()

        //    jrand = 0
        //End Sub

        //Private Sub advance_random()


        //    Dim new_random As Double

        //    For j1 As Integer = 0 To 23

        //        new_random = oldrand(j1) - oldrand(j1 + 31)
        //        If (new_random < 0.0) Then new_random = new_random + 1.0
        //        oldrand(j1) = new_random
        //    Next
        //    For j1 As Integer = 24 To 54

        //        new_random = oldrand(j1) - oldrand(j1 - 24)
        //        If (new_random < 0.0) Then new_random = new_random + 1.0
        //        oldrand(j1) = new_random
        //    Next


        //End Sub

        private double randomperc()
        {

            //jrand += 1
            //If (jrand >= 55) Then

            //    jrand = 1
            //    advance_random()
            //End If
            //Return oldrand(jrand)

            return rnd1.NextDouble();
        }

        private int rnd(int low, int high)
        {

            //Dim i As Integer


            //If (low >= high) Then
            //    i = low
            //Else

            //    i = CInt(randomperc() * (high - low + 1)) + low
            //    If (i > high) Then i = high
            //End If
            //Return i

            return rnd1.Next(low, high);
        }

    }
}