﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EA.Net
{
    public partial class GCUI : UserControl
    {
        private List<Population> _Results;
        private Population _Result;
        private double[] ObjsMax, ObjsMin;
        private string[] _ArrayFO;
        public string[] ArrayFO
        {
            set { _ArrayFO = value; }
            get { return _ArrayFO; }
        }
        public List<Population> Results
        {
            set
            {
                _Results = value;
                if (_Results != null && _Results.Count > 0)
                {
                    ResCurrent = _Results[0];
                    fncAtualiza();
                }
            }
            get { return _Results; }
        }

        public Population Result
        {
            set
            {
                _Result = value;
                if (_Result != null)
                {
                    ResCurrent = _Result;
                    fncAtualiza();
                }
            }
            get { return _Result; }
        }

        private Population ResCurrent;
        private List<ValorIndex> RankCompro;
        public GCUI()
        {
            InitializeComponent();
        }

        private int NrObj, PopSize, NumVars;

        private void GCUI_Load(object sender, EventArgs e)
        {
        }

        public void fncAtualiza()
        {
            if (_Results == null && _Result == null)
            {
                MessageBox.Show("Defina o resultado na propriedade Result ou Results", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Control[] lblF, txtF;
                if (_Result != null && _Results == null)
                {
                    _Results = new List<Population>();
                    _Results.Add(_Result);
                }

                numSimulacao.Minimum = 1;
                numSimulacao.Maximum = _Results.Count;

                // oculta lbls e txts
                NrObj = _Results[0].Ind[0].Objective.Length;
                PopSize = _Results[0].Ind.Length;
                NumVars = _Results[0].Ind[0].Xreal.Length;

                if (_ArrayFO == null)
                {
                    _ArrayFO = new string[NrObj];
                    for (int i = 0; i < NrObj; i++)
                        _ArrayFO[i] = "f" + (i + 1).ToString();
                }

                for (int i = 1; i <= 6; i++)
                {
                    lblF = grpPesos.Panel.Controls.Find("lblF" + i.ToString(), false);
                    ((Elegant.Ui.Label)lblF[0]).Visible = i <= NrObj;

                    txtF = grpPesos.Panel.Controls.Find("txtF" + i.ToString(), false);

                    ((Elegant.Ui.TextBox)txtF[0]).Visible = i <= NrObj;
                    if (i <= NrObj)
                        ((Elegant.Ui.Label)lblF[0]).Text = _ArrayFO[i - 1];
                }
            }

            ResCurrent = _Results[(int)numSimulacao.Value - 1];
            FuncoesGerais.ObjMaxMin(ref ObjsMax, ref ObjsMin, ResCurrent);
            fncCompro();
            fncListaSolucoes();
            fncShowParetoFront();

            EA.Net.GC icc = new EA.Net.GC();
            icc.CalcGC(ResCurrent, NrObj);

            tchGC.Series[0].Clear();
            for (int i = 0; i <= ResCurrent.Ind.Length - 1; i++)
                tchGC.Series[0].Add(ResCurrent.Ind[i].XComp, _Results[0].Ind[i].YComp);

            tchGC.Series[1].Clear();
            for (int i = 0; i <= NrObj - 1; i++)
                PtoComp.Add(ResCurrent.XComp[i], ResCurrent.YComp[i], 0.03, _ArrayFO[i], Color.Red);

            tchGC.Series[2].Clear();
            for (int i = 0; i <= ResCurrent.Ind.Length - 1; i++)
            {
                if (ResCurrent.Ind[i].RankCompro < 10)
                    tchGC.Series[2].Add(ResCurrent.Ind[i].XComp, ResCurrent.Ind[i].YComp);
            }
            PtoGC.Pointer.Pen.Visible = false;
            PtoGC.Pointer.HorizSize = 3;
            PtoGC.Pointer.VertSize = 3;

            if (NrObj == 2)
            {
                ptoGC2D.Clear();
                for (int i = 0; i <= ResCurrent.Ind.Length - 1; i++)
                {
                    if (ResCurrent.Ind[i].RankCompro < 10)
                        ptoGC2D.Add(ResCurrent.Ind[i].Objective[0], ResCurrent.Ind[i].Objective[1]);
                }
            }
            

            spdSolucoes_Sheet1.RowCount = NrObj + NumVars;
            spdSolucoes_Sheet1.ColumnCount = RankCompro.Count;

            for (int i = 0; i < NrObj; i++)
            {
                spdSolucoes_Sheet1.RowHeader.Cells[i, 0].Text = _ArrayFO[i];
            }

            for (int i = 0; i < NumVars; i++)
            {
                spdSolucoes_Sheet1.RowHeader.Cells[i + NrObj, 0].Text = "X" + i.ToString();
            }

            for (int i = 0; i < RankCompro.Count; i++)
            {
                spdSolucoes_Sheet1.ColumnHeader.Cells[0, i].Text = "S" + (i + 1).ToString();
                for (int j = 0; j < NrObj; j++)
                    spdSolucoes_Sheet1.Cells[j, i].Value = ResCurrent.Ind[RankCompro[i].Index].Objective[j].ToString("F5");

                for (int j = 0; j < NumVars; j++)
                    spdSolucoes_Sheet1.Cells[j + NrObj, i].Value = ResCurrent.Ind[RankCompro[i].Index].Xreal[j].ToString("F5");
            }
        }

        private void botAtualiza_Click(object sender, EventArgs e)
        {
            fncAtualiza();
        }

        private void botComputeMetrics_Click(object sender, EventArgs e)
        {

            if (_Results[0].TrueFront == null)
            {
                MessageBox.Show("Para calcular os indicadores, é necessário definir uma frente de Pareto 'Real'", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                ObjsMax = ObjsMin = new double[NrObj];
                double mGD, miGD, mSpread, mDD, mSpacing;
                mGD = miGD = mSpread = mDD = mSpacing = 0;
                Population res;
                Metrics m = new Metrics();
                spdMetrics_Sheet1.RowCount = _Results.Count + 1;
                for (int j = 0; j < _Results.Count; j++)
                {
                    res = _Results[j];

                    FuncoesGerais.ObjMaxMin(ref ObjsMax, ref ObjsMin, res);

                    res.GD = m.GD(res, res.TrueFront, NrObj);
                    res.iGD = m.iGD(res, res.TrueFront, NrObj);
                    if (NrObj < 3)
                        res.Spread = m.ID(res, res.TrueFront, NrObj);
                    res.DD = m.CoverageMetric(res, res.TrueFront, NrObj);
                    res.Spacing = m.Spacing(res, NrObj);

                    spdMetrics_Sheet1.Cells[j, 0].Text = res.GD.ToString();
                    mGD += res.GD;
                    spdMetrics_Sheet1.Cells[j, 1].Text = res.iGD.ToString();
                    miGD += res.iGD;
                    spdMetrics_Sheet1.Cells[j, 2].Text = res.Spread.ToString();
                    mSpread += res.Spread;
                    spdMetrics_Sheet1.Cells[j, 3].Text = res.DD.ToString();
                    mDD += res.DD;
                    spdMetrics_Sheet1.Cells[j, 4].Text = res.Spacing.ToString();
                    mSpacing += res.Spacing;
                    spdMetrics_Sheet1.Cells[j, 5].Text = res.ProcessingTime.ToString();
                }
                spdMetrics_Sheet1.Cells[_Results.Count, 0].Text = (mGD / _Results.Count).ToString();
                spdMetrics_Sheet1.Cells[_Results.Count, 1].Text = (miGD / _Results.Count).ToString();
                spdMetrics_Sheet1.Cells[_Results.Count, 2].Text = (mSpread / _Results.Count).ToString();
                spdMetrics_Sheet1.Cells[_Results.Count, 3].Text = (mDD / _Results.Count).ToString();
                spdMetrics_Sheet1.Cells[_Results.Count, 4].Text = (mSpacing / _Results.Count).ToString();
                spdMetrics_Sheet1.RowHeader.Cells[_Results.Count, 0].Text = "Média";
            }
        }

        private void fncCompro()
        {
            int[] P = new int[3] { 1, 2, 100 };
            double[] Alpha = new double[NrObj];
            double LpTemp = 0;
            Control[] txt;

            List<ValorIndex>[] Lp = new List<ValorIndex>[3];

            //lootp do valor de P
            for (int j = 0; j <= 2; j++)
                Lp[j] = new List<ValorIndex>();

            for (int i = 1; i <= NrObj; i++)
            {
                txt = grpPesos.Panel.Controls.Find("txtF" + i.ToString(), false);
                if (txt != null && txt[0] != null)
                    Alpha[i - 1] = Convert.ToDouble(((Elegant.Ui.TextBox)txt[0]).Text);
            }

            //lootp do valor de P
            for (int j = 0; j <= 2; j++)
            {
                for (int i = 0; i <= PopSize - 1; i++)
                {
                    LpTemp = 0;
                    for (int t = 0; t <= NrObj - 1; t++)
                    {
                        LpTemp += (Math.Pow(Alpha[t], P[j])) * Math.Pow(((ResCurrent.Ind[i].Objective[t] + ObjsMin[t]) / (ObjsMax[t] - ObjsMin[t])), P[j]);
                    }
                    LpTemp = Math.Pow(LpTemp, (1.0 / P[j]));
                    Lp[j].Add(new ValorIndex(LpTemp, i));
                }
            }

            Lp[0].Sort();
            Lp[1].Sort();
            Lp[2].Sort();

            RankCompro = new List<ValorIndex>();

            for (int i = 0; i < ResCurrent.Ind.Count(); i++)
            {
                ResCurrent.Ind[i].RankCompro = 0;
                RankCompro.Add(new ValorIndex(ResCurrent.Ind[i].RankCompro, i));
            }

            for (int i = 0; i < ResCurrent.Ind.Length; i++)
            {
                for (int j = 0; j < 3; j++)
                    RankCompro[Lp[j][i].Index].Valor += (i + 1) / 3.0;
            }
            RankCompro.Sort();

            for (int i = 0; i < RankCompro.Count; i++)
                ResCurrent.Ind[RankCompro[i].Index].RankCompro = i;
            /*
            gridComp_Sheet1.Rows.Clear();
            gridComp_Sheet1.RowCount = PopSize;

            for (int i = 0; i <= PopSize - 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    gridComp_Sheet1.Cells[i, j].Value = (Lp[j][i].Index + 1).ToString();
                }
            }*/
        }


        private void fncShowParetoFront()
        {
            Pto2D.Clear();
            Pto3D.Clear();

            if (NrObj < 3)
            {
                tchGraficos.Walls.View3D = false;

                for (int iSimulation = 0; iSimulation < _Results.Count; iSimulation++)
                {
                    for (int i = 0; i < PopSize; i++)
                        if (_Results[iSimulation].Ind[i].Rank == 1 && _Results[iSimulation].Ind[i].ConstraintViolation == 0)
                            Pto2D.Add(_Results[iSimulation].Ind[i].Objective[0], _Results[iSimulation].Ind[i].Objective[1]);
                }
            }
            else
            {
                tchGraficos.Walls.View3D = true;
                for (int iSimulation = 0; iSimulation < _Results.Count; iSimulation++)
                {
                    //For i As Integer = 0 To mResult(iSimulation).Ind.GetUpperBound(0)
                    //    If mResult(iSimulation).Ind(i).Rank = 1 Then 'AndAlso nsgaResult.ParentPopulation.Ind(i).ConstraintViolation = 0 Then
                    //        Pto3D.Add(mResult(iSimulation).Ind(i).Objective(0), flagMaxMin * mResult(iSimulation).Ind(i).Objective(1), mResult(iSimulation).Ind(i).Objective(2))
                    //    End If
                    //Next
                }
            }
            tchGraficos.Refresh();
        }

        private void fncListaSolucoes()
        {
            spdResults_Sheet1.ColumnCount = NrObj * (_Results.Count + 1) + NrObj;
            spdResults_Sheet1.RowCount = PopSize * _Results.Count + 2 + 300;
            int cnt = 0;

            for (int j = 0; j <= _Results.Count - 1; j++)
            {
                for (int i = 0; i <= NrObj - 1; i++)
                {
                    spdResults_Sheet1.ColumnHeader.Cells[0, j * 2 + i + 2].Text = _ArrayFO[i];
                    spdResults_Sheet1.Columns[i].Width = 150;
                }
                for (int i = 0; i <= _Results[j].Ind.GetUpperBound(0); i++)
                {
                    for (int k = 0; k <= NrObj - 1; k++)
                    {
                        spdResults_Sheet1.Cells[i, j * 2 + k + 2].Value = _Results[j].Ind[i].Objective[k];
                        spdResults_Sheet1.Cells[cnt, k].Value = _Results[j].Ind[i].Objective[k];
                    }
                    cnt += 1;
                }
            }
        }

        private void tchGC_DoubleClick(object sender, EventArgs e)
        {
            tchGC.ShowEditor();
        }

        private void tchGraficos_DoubleClick(object sender, EventArgs e)
        {
            tchGraficos.ShowEditor();
        }
    }
}