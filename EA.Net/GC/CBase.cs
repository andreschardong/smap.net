﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net.CP
{

    [Serializable]
    public class CBase
    {
        // Fields
        private int m_IntVal;
        private float m_SinVal;
        private string m_StrVal;

        // Methods
        public CBase()
        {
        }

        public CBase(string mStrVal, float mSinVal, int mIntVal)
        {
            this.m_StrVal = mStrVal;
            this.m_SinVal = mSinVal;
            this.m_IntVal = mIntVal;
        }

        // Properties
        public int IntVal
        {
            get
            {
                return this.m_IntVal;
            }
            set
            {
                this.m_IntVal = value;
            }
        }

        public float SinVal
        {
            get
            {
                return this.m_SinVal;
            }
            set
            {
                this.m_SinVal = value;
            }
        }

        public string StrVal
        {
            get
            {
                return this.m_StrVal;
            }
            set
            {
                this.m_StrVal = value;
            }
        }
    }
}