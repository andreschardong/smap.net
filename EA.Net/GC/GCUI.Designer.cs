﻿namespace EA.Net
{
    partial class GCUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GCUI));
            FarPoint.Win.Spread.EnhancedScrollBarRenderer enhancedScrollBarRenderer1 = new FarPoint.Win.Spread.EnhancedScrollBarRenderer();
            FarPoint.Win.Spread.EnhancedScrollBarRenderer enhancedScrollBarRenderer2 = new FarPoint.Win.Spread.EnhancedScrollBarRenderer();
            FarPoint.Win.Spread.CellType.NumberCellType numberCellType1 = new FarPoint.Win.Spread.CellType.NumberCellType();
            FarPoint.Win.Spread.CellType.NumberCellType numberCellType2 = new FarPoint.Win.Spread.CellType.NumberCellType();
            FarPoint.Win.Spread.CellType.NumberCellType numberCellType3 = new FarPoint.Win.Spread.CellType.NumberCellType();
            FarPoint.Win.Spread.CellType.NumberCellType numberCellType4 = new FarPoint.Win.Spread.CellType.NumberCellType();
            FarPoint.Win.Spread.CellType.NumberCellType numberCellType5 = new FarPoint.Win.Spread.CellType.NumberCellType();
            FarPoint.Win.Spread.CellType.NumberCellType numberCellType6 = new FarPoint.Win.Spread.CellType.NumberCellType();
            this.grpPesos = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.numSimulacao = new Elegant.Ui.NumericUpDown();
            this.botAtualiza = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label1 = new Elegant.Ui.Label();
            this.lblF6 = new Elegant.Ui.Label();
            this.lblF5 = new Elegant.Ui.Label();
            this.txtF6 = new Elegant.Ui.TextBox();
            this.txtF5 = new Elegant.Ui.TextBox();
            this.lblF4 = new Elegant.Ui.Label();
            this.lblF3 = new Elegant.Ui.Label();
            this.txtF4 = new Elegant.Ui.TextBox();
            this.txtF3 = new Elegant.Ui.TextBox();
            this.lblF2 = new Elegant.Ui.Label();
            this.txtF2 = new Elegant.Ui.TextBox();
            this.lblF1 = new Elegant.Ui.Label();
            this.txtF1 = new Elegant.Ui.TextBox();
            this.tabPrincipal = new Elegant.Ui.TabControl();
            this.tabGC = new Elegant.Ui.TabPage();
            this.tabGraficos = new Elegant.Ui.TabControl();
            this.tabNaoDominadas = new Elegant.Ui.TabPage();
            this.tchGraficos = new Steema.TeeChart.TChart();
            this.Pto2D = new Steema.TeeChart.Styles.Points();
            this.Pto3D = new Steema.TeeChart.Styles.Points3D();
            this.ptoGC2D = new Steema.TeeChart.Styles.Points();
            this.tabCGInterno = new Elegant.Ui.TabPage();
            this.tchGC = new Steema.TeeChart.TChart();
            this.PtoGC = new Steema.TeeChart.Styles.Points();
            this.PtoComp = new Steema.TeeChart.Styles.Bubble();
            this.PtoCompro = new Steema.TeeChart.Styles.Points();
            this.tabComproSol = new Elegant.Ui.TabPage();
            this.spdSolucoes = new FarPoint.Win.Spread.FpSpread();
            this.spdSolucoes_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.tabIndicadores = new Elegant.Ui.TabPage();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.botComputeMetrics = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.spdMetrics = new FarPoint.Win.Spread.FpSpread();
            this.spdMetrics_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.tabTabelas = new Elegant.Ui.TabPage();
            this.botSaveExcel = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.spdResults = new FarPoint.Win.Spread.FpSpread();
            this.spdResults_Sheet1 = new FarPoint.Win.Spread.SheetView();
            ((System.ComponentModel.ISupportInitialize)(this.grpPesos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpPesos.Panel)).BeginInit();
            this.grpPesos.Panel.SuspendLayout();
            this.grpPesos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSimulacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPrincipal)).BeginInit();
            this.tabGC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabGraficos)).BeginInit();
            this.tabNaoDominadas.SuspendLayout();
            this.tabCGInterno.SuspendLayout();
            this.tabComproSol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdSolucoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdSolucoes_Sheet1)).BeginInit();
            this.tabIndicadores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdMetrics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdMetrics_Sheet1)).BeginInit();
            this.tabTabelas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdResults_Sheet1)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPesos
            // 
            resources.ApplyResources(this.grpPesos, "grpPesos");
            this.grpPesos.CaptionStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.grpPesos.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonForm;
            this.grpPesos.Name = "grpPesos";
            // 
            // grpPesos.Panel
            // 
            resources.ApplyResources(this.grpPesos.Panel, "grpPesos.Panel");
            this.grpPesos.Panel.Controls.Add(this.numSimulacao);
            this.grpPesos.Panel.Controls.Add(this.botAtualiza);
            this.grpPesos.Panel.Controls.Add(this.label1);
            this.grpPesos.Panel.Controls.Add(this.lblF6);
            this.grpPesos.Panel.Controls.Add(this.lblF5);
            this.grpPesos.Panel.Controls.Add(this.txtF6);
            this.grpPesos.Panel.Controls.Add(this.txtF5);
            this.grpPesos.Panel.Controls.Add(this.lblF4);
            this.grpPesos.Panel.Controls.Add(this.lblF3);
            this.grpPesos.Panel.Controls.Add(this.txtF4);
            this.grpPesos.Panel.Controls.Add(this.txtF3);
            this.grpPesos.Panel.Controls.Add(this.lblF2);
            this.grpPesos.Panel.Controls.Add(this.txtF2);
            this.grpPesos.Panel.Controls.Add(this.lblF1);
            this.grpPesos.Panel.Controls.Add(this.txtF1);
            this.grpPesos.StateCommon.Back.Color1 = System.Drawing.Color.Transparent;
            this.grpPesos.Values.Description = resources.GetString("grpPesos.Values.Description");
            this.grpPesos.Values.Heading = resources.GetString("grpPesos.Values.Heading");
            this.grpPesos.Values.ImageTransparentColor = ((System.Drawing.Color)(resources.GetObject("grpPesos.Values.ImageTransparentColor")));
            // 
            // numSimulacao
            // 
            resources.ApplyResources(this.numSimulacao, "numSimulacao");
            this.numSimulacao.BannerTextStyle = System.Drawing.FontStyle.Regular;
            this.numSimulacao.Id = "8b96d164-2527-4773-b439-823965dd717d";
            this.numSimulacao.Name = "numSimulacao";
            this.numSimulacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numSimulacao.TextEditorWidth = 19;
            this.numSimulacao.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // botAtualiza
            // 
            resources.ApplyResources(this.botAtualiza, "botAtualiza");
            this.botAtualiza.Name = "botAtualiza";
            this.botAtualiza.Values.ExtraText = resources.GetString("botAtualiza.Values.ExtraText");
            this.botAtualiza.Values.Image = ((System.Drawing.Image)(resources.GetObject("botAtualiza.Values.Image")));
            this.botAtualiza.Values.ImageTransparentColor = ((System.Drawing.Color)(resources.GetObject("botAtualiza.Values.ImageTransparentColor")));
            this.botAtualiza.Values.Text = resources.GetString("botAtualiza.Values.Text");
            this.botAtualiza.Click += new System.EventHandler(this.botAtualiza_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblF6
            // 
            resources.ApplyResources(this.lblF6, "lblF6");
            this.lblF6.Name = "lblF6";
            // 
            // lblF5
            // 
            resources.ApplyResources(this.lblF5, "lblF5");
            this.lblF5.Name = "lblF5";
            // 
            // txtF6
            // 
            resources.ApplyResources(this.txtF6, "txtF6");
            this.txtF6.Id = "413feda9-23b0-4c24-b4be-7db9edbbc032";
            this.txtF6.Name = "txtF6";
            this.txtF6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF6.TextEditorWidth = 32;
            // 
            // txtF5
            // 
            resources.ApplyResources(this.txtF5, "txtF5");
            this.txtF5.Id = "64594df8-9bdf-43e5-b1ed-bb566257e584";
            this.txtF5.Name = "txtF5";
            this.txtF5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF5.TextEditorWidth = 32;
            // 
            // lblF4
            // 
            resources.ApplyResources(this.lblF4, "lblF4");
            this.lblF4.Name = "lblF4";
            // 
            // lblF3
            // 
            resources.ApplyResources(this.lblF3, "lblF3");
            this.lblF3.Name = "lblF3";
            // 
            // txtF4
            // 
            resources.ApplyResources(this.txtF4, "txtF4");
            this.txtF4.Id = "5cbe4ebd-4553-48a9-a5cc-065a06fdc909";
            this.txtF4.Name = "txtF4";
            this.txtF4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF4.TextEditorWidth = 32;
            // 
            // txtF3
            // 
            resources.ApplyResources(this.txtF3, "txtF3");
            this.txtF3.Id = "aeb9ef27-a98c-45ff-a4c9-5328d26db8c0";
            this.txtF3.Name = "txtF3";
            this.txtF3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF3.TextEditorWidth = 32;
            // 
            // lblF2
            // 
            resources.ApplyResources(this.lblF2, "lblF2");
            this.lblF2.Name = "lblF2";
            // 
            // txtF2
            // 
            resources.ApplyResources(this.txtF2, "txtF2");
            this.txtF2.Id = "876cb8df-ec62-4298-a6e4-feaac23fa6dd";
            this.txtF2.Name = "txtF2";
            this.txtF2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF2.TextEditorWidth = 32;
            // 
            // lblF1
            // 
            resources.ApplyResources(this.lblF1, "lblF1");
            this.lblF1.Name = "lblF1";
            // 
            // txtF1
            // 
            resources.ApplyResources(this.txtF1, "txtF1");
            this.txtF1.Id = "4eaaadd5-03ec-4f33-88b1-5f03ca41a0a3";
            this.txtF1.Name = "txtF1";
            this.txtF1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtF1.TextEditorWidth = 32;
            // 
            // tabPrincipal
            // 
            resources.ApplyResources(this.tabPrincipal, "tabPrincipal");
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedTabPage = this.tabIndicadores;
            this.tabPrincipal.TabPages.AddRange(new Elegant.Ui.TabPage[] {
            this.tabGC,
            this.tabIndicadores,
            this.tabTabelas});
            // 
            // tabGC
            // 
            resources.ApplyResources(this.tabGC, "tabGC");
            this.tabGC.ActiveControl = null;
            this.tabGC.Controls.Add(this.tabGraficos);
            this.tabGC.Controls.Add(this.grpPesos);
            this.tabGC.KeyTip = null;
            this.tabGC.Name = "tabGC";
            // 
            // tabGraficos
            // 
            resources.ApplyResources(this.tabGraficos, "tabGraficos");
            this.tabGraficos.Name = "tabGraficos";
            this.tabGraficos.SelectedTabPage = this.tabNaoDominadas;
            this.tabGraficos.TabPages.AddRange(new Elegant.Ui.TabPage[] {
            this.tabNaoDominadas,
            this.tabCGInterno,
            this.tabComproSol});
            // 
            // tabNaoDominadas
            // 
            resources.ApplyResources(this.tabNaoDominadas, "tabNaoDominadas");
            this.tabNaoDominadas.ActiveControl = null;
            this.tabNaoDominadas.Controls.Add(this.tchGraficos);
            this.tabNaoDominadas.KeyTip = null;
            this.tabNaoDominadas.Name = "tabNaoDominadas";
            // 
            // tchGraficos
            // 
            resources.ApplyResources(this.tchGraficos, "tchGraficos");
            // 
            // 
            // 
            this.tchGraficos.Aspect.Chart3DPercent = 30;
            this.tchGraficos.Aspect.View3D = false;
            this.tchGraficos.Aspect.ZOffset = 0D;
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Axes.Bottom.Labels.Font.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tchGraficos.Axes.Bottom.Labels.Font.Name = "Cambria";
            this.tchGraficos.Axes.Bottom.Labels.Font.Size = 9;
            this.tchGraficos.Axes.Bottom.Labels.Font.SizeFloat = 9F;
            this.tchGraficos.Axes.Bottom.MaximumOffset = 5;
            this.tchGraficos.Axes.Bottom.MinimumOffset = 5;
            // 
            // 
            // 
            this.tchGraficos.Axes.Bottom.Title.Caption = "F1";
            // 
            // 
            // 
            this.tchGraficos.Axes.Bottom.Title.Font.Name = "Cambria";
            this.tchGraficos.Axes.Bottom.Title.Font.Size = 9;
            this.tchGraficos.Axes.Bottom.Title.Font.SizeFloat = 9F;
            this.tchGraficos.Axes.Bottom.Title.Lines = new string[] {
        "F1"};
            this.tchGraficos.Axes.Bottom.Title.Transparent = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Axes.Depth.Title.Transparent = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Axes.DepthTop.Title.Transparent = true;
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Axes.Left.Labels.Font.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tchGraficos.Axes.Left.Labels.Font.Name = "Cambria";
            this.tchGraficos.Axes.Left.Labels.Font.Size = 9;
            this.tchGraficos.Axes.Left.Labels.Font.SizeFloat = 9F;
            this.tchGraficos.Axes.Left.MaximumOffset = 5;
            this.tchGraficos.Axes.Left.MinimumOffset = 5;
            // 
            // 
            // 
            this.tchGraficos.Axes.Left.Title.Caption = "F2";
            // 
            // 
            // 
            this.tchGraficos.Axes.Left.Title.Font.Name = "Cambria";
            this.tchGraficos.Axes.Left.Title.Font.Size = 9;
            this.tchGraficos.Axes.Left.Title.Font.SizeFloat = 9F;
            this.tchGraficos.Axes.Left.Title.Lines = new string[] {
        "F2"};
            this.tchGraficos.Axes.Left.Title.Transparent = true;
            // 
            // 
            // 
            this.tchGraficos.Axes.Right.Automatic = false;
            this.tchGraficos.Axes.Right.AutomaticMaximum = false;
            this.tchGraficos.Axes.Right.AutomaticMinimum = false;
            this.tchGraficos.Axes.Right.Inverted = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Axes.Right.Labels.Font.Name = "Cambria";
            this.tchGraficos.Axes.Right.Maximum = 2000D;
            this.tchGraficos.Axes.Right.MaximumOffset = 37;
            this.tchGraficos.Axes.Right.Minimum = 0D;
            // 
            // 
            // 
            this.tchGraficos.Axes.Right.Title.Transparent = true;
            this.tchGraficos.Axes.Right.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Axes.Top.Title.Transparent = true;
            this.tchGraficos.Axes.Top.Visible = false;
            this.tchGraficos.BackColor = System.Drawing.Color.Transparent;
            this.tchGraficos.Cursor = System.Windows.Forms.Cursors.Default;
            // 
            // 
            // 
            this.tchGraficos.Header.Lines = new string[] {
        ""};
            this.tchGraficos.Header.Visible = false;
            // 
            // 
            // 
            this.tchGraficos.Legend.Alignment = Steema.TeeChart.LegendAlignments.Bottom;
            // 
            // 
            // 
            this.tchGraficos.Legend.Bevel.ColorTwo = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Legend.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.tchGraficos.Legend.Brush.Gradient.Visible = true;
            this.tchGraficos.Legend.Visible = false;
            this.tchGraficos.Name = "tchGraficos";
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGraficos.Panel.Bevel.ColorOne = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tchGraficos.Panel.Bevel.ColorTwo = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tchGraficos.Panel.Bevel.Inner = Steema.TeeChart.Drawing.BevelStyles.Lowered;
            // 
            // 
            // 
            this.tchGraficos.Panel.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(215)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.tchGraficos.Panel.Brush.Gradient.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(249)))), ((int)(((byte)(251)))));
            this.tchGraficos.Panel.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tchGraficos.Panel.Brush.Gradient.UseMiddle = false;
            this.tchGraficos.Panel.Brush.Gradient.Visible = false;
            this.tchGraficos.Series.Add(this.Pto2D);
            this.tchGraficos.Series.Add(this.Pto3D);
            this.tchGraficos.Series.Add(this.ptoGC2D);
            // 
            // 
            // 
            this.tchGraficos.Walls.Visible = false;
            this.tchGraficos.DoubleClick += new System.EventHandler(this.tchGraficos_DoubleClick);
            // 
            // Pto2D
            // 
            this.Pto2D.Color = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(102)))), ((int)(((byte)(163)))));
            this.Pto2D.ColorEach = false;
            // 
            // 
            // 
            this.Pto2D.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(61)))), ((int)(((byte)(98)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.Pto2D.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
            this.Pto2D.Marks.Callout.ArrowHeadSize = 8;
            // 
            // 
            // 
            this.Pto2D.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
            this.Pto2D.Marks.Callout.Distance = 0;
            this.Pto2D.Marks.Callout.Draw3D = false;
            this.Pto2D.Marks.Callout.Length = 0;
            this.Pto2D.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
            this.Pto2D.Marks.Callout.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.Pto2D.Pointer.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(102)))), ((int)(((byte)(163)))));
            // 
            // 
            // 
            this.Pto2D.Pointer.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(102)))), ((int)(((byte)(163)))));
            this.Pto2D.Pointer.Dark3D = false;
            this.Pto2D.Pointer.Draw3D = false;
            this.Pto2D.Pointer.HorizSize = 3;
            // 
            // 
            // 
            this.Pto2D.Pointer.Pen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(61)))), ((int)(((byte)(98)))));
            this.Pto2D.Pointer.Pen.Visible = false;
            this.Pto2D.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Circle;
            this.Pto2D.Pointer.VertSize = 3;
            resources.ApplyResources(this.Pto2D, "Pto2D");
            // 
            // 
            // 
            this.Pto2D.XValues.DataMember = "X";
            this.Pto2D.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
            // 
            // 
            // 
            this.Pto2D.YValues.DataMember = "Y";
            // 
            // Pto3D
            // 
            this.Pto3D.Color = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(53)))));
            this.Pto3D.ColorEach = false;
            // 
            // 
            // 
            this.Pto3D.LinePen.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.Pto3D.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
            this.Pto3D.Marks.Callout.ArrowHeadSize = 8;
            // 
            // 
            // 
            this.Pto3D.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
            this.Pto3D.Marks.Callout.Distance = 0;
            this.Pto3D.Marks.Callout.Draw3D = false;
            this.Pto3D.Marks.Callout.Length = 10;
            this.Pto3D.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
            this.Pto3D.Marks.Callout.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.Pto3D.Pointer.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(53)))));
            // 
            // 
            // 
            this.Pto3D.Pointer.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(53)))));
            this.Pto3D.Pointer.Dark3D = false;
            this.Pto3D.Pointer.Draw3D = false;
            this.Pto3D.Pointer.HorizSize = 3;
            // 
            // 
            // 
            this.Pto3D.Pointer.Pen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(94)))), ((int)(((byte)(32)))));
            this.Pto3D.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Circle;
            this.Pto3D.Pointer.VertSize = 3;
            resources.ApplyResources(this.Pto3D, "Pto3D");
            // 
            // 
            // 
            this.Pto3D.XValues.DataMember = "X";
            // 
            // 
            // 
            this.Pto3D.YValues.DataMember = "Y";
            // 
            // 
            // 
            this.Pto3D.ZValues.DataMember = "Z";
            // 
            // ptoGC2D
            // 
            this.ptoGC2D.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ptoGC2D.ColorEach = false;
            // 
            // 
            // 
            this.ptoGC2D.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(46)))), ((int)(((byte)(12)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.ptoGC2D.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
            this.ptoGC2D.Marks.Callout.ArrowHeadSize = 8;
            // 
            // 
            // 
            this.ptoGC2D.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
            this.ptoGC2D.Marks.Callout.Distance = 0;
            this.ptoGC2D.Marks.Callout.Draw3D = false;
            this.ptoGC2D.Marks.Callout.Length = 0;
            this.ptoGC2D.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
            this.ptoGC2D.Marks.Callout.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.ptoGC2D.Pointer.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            // 
            // 
            // 
            this.ptoGC2D.Pointer.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(76)))), ((int)(((byte)(20)))));
            this.ptoGC2D.Pointer.Dark3D = false;
            this.ptoGC2D.Pointer.Draw3D = false;
            // 
            // 
            // 
            this.ptoGC2D.Pointer.Pen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ptoGC2D.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Circle;
            resources.ApplyResources(this.ptoGC2D, "ptoGC2D");
            // 
            // 
            // 
            this.ptoGC2D.XValues.DataMember = "X";
            this.ptoGC2D.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
            // 
            // 
            // 
            this.ptoGC2D.YValues.DataMember = "Y";
            // 
            // tabCGInterno
            // 
            resources.ApplyResources(this.tabCGInterno, "tabCGInterno");
            this.tabCGInterno.ActiveControl = null;
            this.tabCGInterno.Controls.Add(this.tchGC);
            this.tabCGInterno.KeyTip = null;
            this.tabCGInterno.Name = "tabCGInterno";
            // 
            // tchGC
            // 
            resources.ApplyResources(this.tchGC, "tchGC");
            // 
            // 
            // 
            this.tchGC.Aspect.Chart3DPercent = 30;
            this.tchGC.Aspect.View3D = false;
            this.tchGC.Aspect.ZOffset = 0D;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.Bottom.Automatic = false;
            this.tchGC.Axes.Bottom.AutomaticMaximum = false;
            this.tchGC.Axes.Bottom.AutomaticMinimum = false;
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.Bottom.Labels.Font.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tchGC.Axes.Bottom.Labels.Font.Name = "Cambria";
            this.tchGC.Axes.Bottom.Labels.Font.Size = 9;
            this.tchGC.Axes.Bottom.Labels.Font.SizeFloat = 9F;
            this.tchGC.Axes.Bottom.Maximum = 1.1D;
            this.tchGC.Axes.Bottom.MaximumOffset = 5;
            this.tchGC.Axes.Bottom.Minimum = -1.1D;
            this.tchGC.Axes.Bottom.MinimumOffset = 5;
            // 
            // 
            // 
            this.tchGC.Axes.Bottom.Title.Caption = "F1";
            // 
            // 
            // 
            this.tchGC.Axes.Bottom.Title.Font.Name = "Cambria";
            this.tchGC.Axes.Bottom.Title.Font.Size = 9;
            this.tchGC.Axes.Bottom.Title.Font.SizeFloat = 9F;
            this.tchGC.Axes.Bottom.Title.Lines = new string[] {
        "F1"};
            this.tchGC.Axes.Bottom.Title.Transparent = true;
            this.tchGC.Axes.Bottom.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.Depth.Title.Transparent = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.DepthTop.Title.Transparent = true;
            // 
            // 
            // 
            this.tchGC.Axes.Left.Automatic = false;
            this.tchGC.Axes.Left.AutomaticMaximum = false;
            this.tchGC.Axes.Left.AutomaticMinimum = false;
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.Left.Labels.Font.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tchGC.Axes.Left.Labels.Font.Name = "Cambria";
            this.tchGC.Axes.Left.Labels.Font.Size = 9;
            this.tchGC.Axes.Left.Labels.Font.SizeFloat = 9F;
            this.tchGC.Axes.Left.Maximum = 1.1D;
            this.tchGC.Axes.Left.MaximumOffset = 5;
            this.tchGC.Axes.Left.Minimum = -1.1D;
            this.tchGC.Axes.Left.MinimumOffset = 5;
            // 
            // 
            // 
            this.tchGC.Axes.Left.Title.Caption = "F2";
            // 
            // 
            // 
            this.tchGC.Axes.Left.Title.Font.Name = "Cambria";
            this.tchGC.Axes.Left.Title.Font.Size = 9;
            this.tchGC.Axes.Left.Title.Font.SizeFloat = 9F;
            this.tchGC.Axes.Left.Title.Lines = new string[] {
        "F2"};
            this.tchGC.Axes.Left.Title.Transparent = true;
            this.tchGC.Axes.Left.Visible = false;
            // 
            // 
            // 
            this.tchGC.Axes.Right.Inverted = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.Right.Labels.Font.Name = "Cambria";
            this.tchGC.Axes.Right.MaximumOffset = 37;
            // 
            // 
            // 
            this.tchGC.Axes.Right.Title.Transparent = true;
            this.tchGC.Axes.Right.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Axes.Top.Title.Transparent = true;
            this.tchGC.Axes.Top.Visible = false;
            this.tchGC.BackColor = System.Drawing.Color.Transparent;
            this.tchGC.Cursor = System.Windows.Forms.Cursors.Default;
            // 
            // 
            // 
            this.tchGC.Header.Lines = new string[] {
        ""};
            this.tchGC.Header.Visible = false;
            // 
            // 
            // 
            this.tchGC.Legend.Alignment = Steema.TeeChart.LegendAlignments.Bottom;
            // 
            // 
            // 
            this.tchGC.Legend.Bevel.ColorTwo = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Legend.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.tchGC.Legend.Brush.Gradient.Visible = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Legend.Font.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(200)))), ((int)(((byte)(220)))));
            this.tchGC.Legend.Visible = false;
            this.tchGC.Name = "tchGC";
            // 
            // 
            // 
            // 
            // 
            // 
            this.tchGC.Panel.Bevel.ColorOne = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tchGC.Panel.Bevel.ColorTwo = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tchGC.Panel.Bevel.Inner = Steema.TeeChart.Drawing.BevelStyles.Lowered;
            // 
            // 
            // 
            this.tchGC.Panel.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.tchGC.Panel.Brush.Gradient.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(249)))), ((int)(((byte)(251)))));
            this.tchGC.Panel.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tchGC.Panel.Brush.Gradient.UseMiddle = false;
            this.tchGC.Panel.Brush.Gradient.Visible = false;
            this.tchGC.Series.Add(this.PtoGC);
            this.tchGC.Series.Add(this.PtoComp);
            this.tchGC.Series.Add(this.PtoCompro);
            // 
            // 
            // 
            this.tchGC.Walls.Visible = false;
            this.tchGC.DoubleClick += new System.EventHandler(this.tchGC_DoubleClick);
            // 
            // PtoGC
            // 
            this.PtoGC.Color = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(200)))), ((int)(((byte)(220)))));
            this.PtoGC.ColorEach = false;
            // 
            // 
            // 
            this.PtoGC.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(61)))), ((int)(((byte)(98)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.PtoGC.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
            this.PtoGC.Marks.Callout.ArrowHeadSize = 8;
            // 
            // 
            // 
            this.PtoGC.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
            this.PtoGC.Marks.Callout.Distance = 0;
            this.PtoGC.Marks.Callout.Draw3D = false;
            this.PtoGC.Marks.Callout.Length = 0;
            this.PtoGC.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
            this.PtoGC.Marks.Callout.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.PtoGC.Pointer.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(200)))), ((int)(((byte)(220)))));
            // 
            // 
            // 
            this.PtoGC.Pointer.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(102)))), ((int)(((byte)(163)))));
            this.PtoGC.Pointer.Dark3D = false;
            this.PtoGC.Pointer.Draw3D = false;
            this.PtoGC.Pointer.HorizSize = 3;
            this.PtoGC.Pointer.InflateMargins = false;
            // 
            // 
            // 
            this.PtoGC.Pointer.Pen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(61)))), ((int)(((byte)(98)))));
            this.PtoGC.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Circle;
            this.PtoGC.Pointer.VertSize = 3;
            resources.ApplyResources(this.PtoGC, "PtoGC");
            // 
            // 
            // 
            this.PtoGC.XValues.DataMember = "X";
            this.PtoGC.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
            // 
            // 
            // 
            this.PtoGC.YValues.DataMember = "Y";
            // 
            // PtoComp
            // 
            this.PtoComp.Color = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(63)))), ((int)(((byte)(155)))));
            // 
            // 
            // 
            this.PtoComp.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(38)))), ((int)(((byte)(93)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.PtoComp.Marks.Brush.Visible = false;
            // 
            // 
            // 
            this.PtoComp.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
            this.PtoComp.Marks.Callout.ArrowHeadSize = 8;
            // 
            // 
            // 
            this.PtoComp.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
            this.PtoComp.Marks.Callout.Distance = 0;
            this.PtoComp.Marks.Callout.Draw3D = false;
            this.PtoComp.Marks.Callout.Length = 0;
            this.PtoComp.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
            this.PtoComp.Marks.Callout.Visible = false;
            // 
            // 
            // 
            this.PtoComp.Marks.Font.Name = "Cambria";
            // 
            // 
            // 
            this.PtoComp.Marks.Font.Shadow.Visible = true;
            this.PtoComp.Marks.Font.Size = 9;
            this.PtoComp.Marks.Font.SizeFloat = 9F;
            this.PtoComp.Marks.Transparent = true;
            this.PtoComp.Marks.Visible = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.PtoComp.Pointer.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(63)))), ((int)(((byte)(155)))));
            this.PtoComp.Pointer.Brush.ForegroundColor = System.Drawing.Color.Empty;
            // 
            // 
            // 
            this.PtoComp.Pointer.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(63)))), ((int)(((byte)(155)))));
            this.PtoComp.Pointer.Dark3D = false;
            this.PtoComp.Pointer.Draw3D = false;
            this.PtoComp.Pointer.HorizSize = 10415;
            this.PtoComp.Pointer.InflateMargins = false;
            this.PtoComp.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Circle;
            this.PtoComp.Pointer.VertSize = 10415;
            resources.ApplyResources(this.PtoComp, "PtoComp");
            // 
            // 
            // 
            this.PtoComp.XValues.DataMember = "X";
            this.PtoComp.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
            // 
            // 
            // 
            this.PtoComp.YValues.DataMember = "Y";
            // 
            // PtoCompro
            // 
            this.PtoCompro.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(147)))), ((int)(((byte)(0)))));
            this.PtoCompro.ColorEach = false;
            // 
            // 
            // 
            this.PtoCompro.LinePen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(61)))), ((int)(((byte)(98)))));
            // 
            // 
            // 
            this.PtoCompro.Marks.ArrowLength = 10;
            // 
            // 
            // 
            this.PtoCompro.Marks.Callout.ArrowHead = Steema.TeeChart.Styles.ArrowHeadStyles.None;
            this.PtoCompro.Marks.Callout.ArrowHeadSize = 8;
            // 
            // 
            // 
            this.PtoCompro.Marks.Callout.Brush.Color = System.Drawing.Color.Black;
            this.PtoCompro.Marks.Callout.Distance = 0;
            this.PtoCompro.Marks.Callout.Draw3D = false;
            this.PtoCompro.Marks.Callout.Length = 10;
            this.PtoCompro.Marks.Callout.Style = Steema.TeeChart.Styles.PointerStyles.Rectangle;
            this.PtoCompro.Marks.Callout.Visible = false;
            // 
            // 
            // 
            // 
            // 
            // 
            this.PtoCompro.Pointer.Brush.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(147)))), ((int)(((byte)(0)))));
            // 
            // 
            // 
            this.PtoCompro.Pointer.Brush.Gradient.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(102)))), ((int)(((byte)(163)))));
            this.PtoCompro.Pointer.Dark3D = false;
            this.PtoCompro.Pointer.Draw3D = false;
            this.PtoCompro.Pointer.InflateMargins = false;
            // 
            // 
            // 
            this.PtoCompro.Pointer.Pen.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(61)))), ((int)(((byte)(98)))));
            this.PtoCompro.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Circle;
            resources.ApplyResources(this.PtoCompro, "PtoCompro");
            // 
            // 
            // 
            this.PtoCompro.XValues.DataMember = "X";
            this.PtoCompro.XValues.Order = Steema.TeeChart.Styles.ValueListOrder.Ascending;
            // 
            // 
            // 
            this.PtoCompro.YValues.DataMember = "Y";
            // 
            // tabComproSol
            // 
            resources.ApplyResources(this.tabComproSol, "tabComproSol");
            this.tabComproSol.ActiveControl = null;
            this.tabComproSol.Controls.Add(this.spdSolucoes);
            this.tabComproSol.KeyTip = null;
            this.tabComproSol.Name = "tabComproSol";
            // 
            // spdSolucoes
            // 
            resources.ApplyResources(this.spdSolucoes, "spdSolucoes");
            this.spdSolucoes.HorizontalScrollBar.AccessibleDescription = resources.GetString("resource.AccessibleDescription");
            this.spdSolucoes.HorizontalScrollBar.AccessibleName = resources.GetString("resource.AccessibleName");
            this.spdSolucoes.HorizontalScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("resource.Anchor")));
            this.spdSolucoes.HorizontalScrollBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("resource.BackgroundImage")));
            this.spdSolucoes.HorizontalScrollBar.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("resource.BackgroundImageLayout")));
            this.spdSolucoes.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spdSolucoes.HorizontalScrollBar.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("resource.Dock")));
            this.spdSolucoes.HorizontalScrollBar.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font")));
            this.spdSolucoes.HorizontalScrollBar.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode")));
            this.spdSolucoes.HorizontalScrollBar.MaximumSize = ((System.Drawing.Size)(resources.GetObject("resource.MaximumSize")));
            this.spdSolucoes.HorizontalScrollBar.Name = "";
            enhancedScrollBarRenderer1.ArrowColor = System.Drawing.Color.Black;
            enhancedScrollBarRenderer1.ArrowHoveredColor = System.Drawing.Color.Black;
            enhancedScrollBarRenderer1.ArrowSelectedColor = System.Drawing.Color.Black;
            enhancedScrollBarRenderer1.ButtonBackgroundColor = System.Drawing.Color.Silver;
            enhancedScrollBarRenderer1.ButtonBorderColor = System.Drawing.Color.DarkGray;
            enhancedScrollBarRenderer1.ButtonHoveredBackgroundColor = System.Drawing.Color.DarkGray;
            enhancedScrollBarRenderer1.ButtonHoveredBorderColor = System.Drawing.Color.DimGray;
            enhancedScrollBarRenderer1.ButtonSelectedBackgroundColor = System.Drawing.Color.DimGray;
            enhancedScrollBarRenderer1.ButtonSelectedBorderColor = System.Drawing.Color.Gray;
            enhancedScrollBarRenderer1.TrackBarBackgroundColor = System.Drawing.Color.DarkGray;
            enhancedScrollBarRenderer1.TrackBarSelectedBackgroundColor = System.Drawing.Color.Gray;
            this.spdSolucoes.HorizontalScrollBar.Renderer = enhancedScrollBarRenderer1;
            this.spdSolucoes.HorizontalScrollBar.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("resource.RightToLeft")));
            this.spdSolucoes.HorizontalScrollBar.TabIndex = ((int)(resources.GetObject("resource.TabIndex")));
            this.spdSolucoes.Name = "spdSolucoes";
            this.spdSolucoes.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spdSolucoes_Sheet1});
            this.spdSolucoes.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Metallic;
            this.spdSolucoes.VerticalScrollBar.AccessibleDescription = resources.GetString("resource.AccessibleDescription1");
            this.spdSolucoes.VerticalScrollBar.AccessibleName = resources.GetString("resource.AccessibleName1");
            this.spdSolucoes.VerticalScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("resource.Anchor1")));
            this.spdSolucoes.VerticalScrollBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("resource.BackgroundImage1")));
            this.spdSolucoes.VerticalScrollBar.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("resource.BackgroundImageLayout1")));
            this.spdSolucoes.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spdSolucoes.VerticalScrollBar.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("resource.Dock1")));
            this.spdSolucoes.VerticalScrollBar.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font1")));
            this.spdSolucoes.VerticalScrollBar.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode1")));
            this.spdSolucoes.VerticalScrollBar.MaximumSize = ((System.Drawing.Size)(resources.GetObject("resource.MaximumSize1")));
            this.spdSolucoes.VerticalScrollBar.Name = "";
            enhancedScrollBarRenderer2.ArrowColor = System.Drawing.Color.Black;
            enhancedScrollBarRenderer2.ArrowHoveredColor = System.Drawing.Color.Black;
            enhancedScrollBarRenderer2.ArrowSelectedColor = System.Drawing.Color.Black;
            enhancedScrollBarRenderer2.ButtonBackgroundColor = System.Drawing.Color.Silver;
            enhancedScrollBarRenderer2.ButtonBorderColor = System.Drawing.Color.DarkGray;
            enhancedScrollBarRenderer2.ButtonHoveredBackgroundColor = System.Drawing.Color.DarkGray;
            enhancedScrollBarRenderer2.ButtonHoveredBorderColor = System.Drawing.Color.DimGray;
            enhancedScrollBarRenderer2.ButtonSelectedBackgroundColor = System.Drawing.Color.DimGray;
            enhancedScrollBarRenderer2.ButtonSelectedBorderColor = System.Drawing.Color.Gray;
            enhancedScrollBarRenderer2.TrackBarBackgroundColor = System.Drawing.Color.DarkGray;
            enhancedScrollBarRenderer2.TrackBarSelectedBackgroundColor = System.Drawing.Color.Gray;
            this.spdSolucoes.VerticalScrollBar.Renderer = enhancedScrollBarRenderer2;
            this.spdSolucoes.VerticalScrollBar.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("resource.RightToLeft1")));
            this.spdSolucoes.VerticalScrollBar.TabIndex = ((int)(resources.GetObject("resource.TabIndex1")));
            // 
            // spdSolucoes_Sheet1
            // 
            this.spdSolucoes_Sheet1.Reset();
            this.spdSolucoes_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spdSolucoes_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spdSolucoes_Sheet1.ColumnCount = 10;
            this.spdSolucoes_Sheet1.RowCount = 1;
            this.spdSolucoes_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spdSolucoes_Sheet1.ColumnFooter.DefaultStyle.Parent = "ColumnHeaderMetallic";
            this.spdSolucoes_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spdSolucoes_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerMetallic";
            this.spdSolucoes_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spdSolucoes_Sheet1.ColumnHeader.DefaultStyle.Parent = "ColumnHeaderMetallic";
            this.spdSolucoes_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spdSolucoes_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spdSolucoes_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderMetallic";
            this.spdSolucoes_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spdSolucoes_Sheet1.SheetCornerStyle.Parent = "CornerMetallic";
            this.spdSolucoes_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // tabIndicadores
            // 
            resources.ApplyResources(this.tabIndicadores, "tabIndicadores");
            this.tabIndicadores.ActiveControl = null;
            this.tabIndicadores.Controls.Add(this.kryptonButton1);
            this.tabIndicadores.Controls.Add(this.botComputeMetrics);
            this.tabIndicadores.Controls.Add(this.spdMetrics);
            this.tabIndicadores.KeyTip = null;
            this.tabIndicadores.Name = "tabIndicadores";
            // 
            // kryptonButton1
            // 
            resources.ApplyResources(this.kryptonButton1, "kryptonButton1");
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.Values.ExtraText = resources.GetString("kryptonButton1.Values.ExtraText");
            this.kryptonButton1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton1.Values.Image")));
            this.kryptonButton1.Values.ImageTransparentColor = ((System.Drawing.Color)(resources.GetObject("kryptonButton1.Values.ImageTransparentColor")));
            this.kryptonButton1.Values.Text = resources.GetString("kryptonButton1.Values.Text");
            this.kryptonButton1.Click += new System.EventHandler(this.botComputeMetrics_Click);
            // 
            // botComputeMetrics
            // 
            resources.ApplyResources(this.botComputeMetrics, "botComputeMetrics");
            this.botComputeMetrics.Name = "botComputeMetrics";
            this.botComputeMetrics.Values.ExtraText = resources.GetString("botComputeMetrics.Values.ExtraText");
            this.botComputeMetrics.Values.Image = ((System.Drawing.Image)(resources.GetObject("botComputeMetrics.Values.Image")));
            this.botComputeMetrics.Values.ImageTransparentColor = ((System.Drawing.Color)(resources.GetObject("botComputeMetrics.Values.ImageTransparentColor")));
            this.botComputeMetrics.Values.Text = resources.GetString("botComputeMetrics.Values.Text");
            this.botComputeMetrics.Click += new System.EventHandler(this.botComputeMetrics_Click);
            // 
            // spdMetrics
            // 
            resources.ApplyResources(this.spdMetrics, "spdMetrics");
            this.spdMetrics.Name = "spdMetrics";
            this.spdMetrics.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spdMetrics_Sheet1});
            // 
            // spdMetrics_Sheet1
            // 
            this.spdMetrics_Sheet1.Reset();
            this.spdMetrics_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spdMetrics_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spdMetrics_Sheet1.ColumnCount = 6;
            this.spdMetrics_Sheet1.RowCount = 1;
            this.spdMetrics_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "GD";
            this.spdMetrics_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "iGD";
            this.spdMetrics_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "Spread";
            this.spdMetrics_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Coverage";
            this.spdMetrics_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "Spacing";
            this.spdMetrics_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "Proc. Time";
            numberCellType1.DecimalPlaces = 6;
            this.spdMetrics_Sheet1.Columns.Get(0).CellType = numberCellType1;
            this.spdMetrics_Sheet1.Columns.Get(0).Label = "GD";
            numberCellType2.DecimalPlaces = 6;
            this.spdMetrics_Sheet1.Columns.Get(1).CellType = numberCellType2;
            this.spdMetrics_Sheet1.Columns.Get(1).Label = "iGD";
            numberCellType3.DecimalPlaces = 6;
            this.spdMetrics_Sheet1.Columns.Get(2).CellType = numberCellType3;
            this.spdMetrics_Sheet1.Columns.Get(2).Label = "Spread";
            numberCellType4.DecimalPlaces = 6;
            this.spdMetrics_Sheet1.Columns.Get(3).CellType = numberCellType4;
            this.spdMetrics_Sheet1.Columns.Get(3).Label = "Coverage";
            numberCellType5.DecimalPlaces = 6;
            this.spdMetrics_Sheet1.Columns.Get(4).CellType = numberCellType5;
            this.spdMetrics_Sheet1.Columns.Get(4).Label = "Spacing";
            numberCellType6.DecimalPlaces = 0;
            this.spdMetrics_Sheet1.Columns.Get(5).CellType = numberCellType6;
            this.spdMetrics_Sheet1.Columns.Get(5).Label = "Proc. Time";
            this.spdMetrics_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spdMetrics_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // tabTabelas
            // 
            resources.ApplyResources(this.tabTabelas, "tabTabelas");
            this.tabTabelas.ActiveControl = null;
            this.tabTabelas.Controls.Add(this.botSaveExcel);
            this.tabTabelas.Controls.Add(this.spdResults);
            this.tabTabelas.KeyTip = null;
            this.tabTabelas.Name = "tabTabelas";
            // 
            // botSaveExcel
            // 
            resources.ApplyResources(this.botSaveExcel, "botSaveExcel");
            this.botSaveExcel.Name = "botSaveExcel";
            this.botSaveExcel.Values.ExtraText = resources.GetString("botSaveExcel.Values.ExtraText");
            this.botSaveExcel.Values.Image = ((System.Drawing.Image)(resources.GetObject("botSaveExcel.Values.Image")));
            this.botSaveExcel.Values.ImageTransparentColor = ((System.Drawing.Color)(resources.GetObject("botSaveExcel.Values.ImageTransparentColor")));
            this.botSaveExcel.Values.Text = resources.GetString("botSaveExcel.Values.Text");
            // 
            // spdResults
            // 
            resources.ApplyResources(this.spdResults, "spdResults");
            this.spdResults.Name = "spdResults";
            this.spdResults.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spdResults_Sheet1});
            // 
            // spdResults_Sheet1
            // 
            this.spdResults_Sheet1.Reset();
            this.spdResults_Sheet1.SheetName = "Sheet1";
            // 
            // GCUI
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tabPrincipal);
            this.Name = "GCUI";
            this.Load += new System.EventHandler(this.GCUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpPesos.Panel)).EndInit();
            this.grpPesos.Panel.ResumeLayout(false);
            this.grpPesos.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpPesos)).EndInit();
            this.grpPesos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numSimulacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPrincipal)).EndInit();
            this.tabGC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabGraficos)).EndInit();
            this.tabNaoDominadas.ResumeLayout(false);
            this.tabCGInterno.ResumeLayout(false);
            this.tabComproSol.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spdSolucoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdSolucoes_Sheet1)).EndInit();
            this.tabIndicadores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spdMetrics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdMetrics_Sheet1)).EndInit();
            this.tabTabelas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spdResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdResults_Sheet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonButton botAtualiza;
        internal Elegant.Ui.TabControl tabPrincipal;
        private Elegant.Ui.TabPage tabGC;
        private Elegant.Ui.TabPage tabTabelas;
        internal Elegant.Ui.Label lblF6;
        internal Elegant.Ui.Label lblF5;
        internal Elegant.Ui.TextBox txtF6;
        internal Elegant.Ui.TextBox txtF5;
        internal Elegant.Ui.Label lblF4;
        internal Elegant.Ui.Label lblF3;
        internal Elegant.Ui.TextBox txtF4;
        internal Elegant.Ui.TextBox txtF3;
        internal Elegant.Ui.Label lblF2;
        internal Elegant.Ui.TextBox txtF2;
        internal Elegant.Ui.Label lblF1;
        internal Elegant.Ui.TextBox txtF1;
        internal Elegant.Ui.TabControl tabGraficos;
        private ComponentFactory.Krypton.Toolkit.KryptonButton botSaveExcel;
        internal FarPoint.Win.Spread.FpSpread spdResults;
        internal FarPoint.Win.Spread.SheetView spdResults_Sheet1;
        private Elegant.Ui.TabPage tabCGInterno;
        internal Steema.TeeChart.TChart tchGC;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox grpPesos;
        private Elegant.Ui.TabPage tabIndicadores;
        internal FarPoint.Win.Spread.FpSpread spdMetrics;
        internal FarPoint.Win.Spread.SheetView spdMetrics_Sheet1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton botComputeMetrics;
        internal Steema.TeeChart.Styles.Points PtoCompro;
        internal Steema.TeeChart.Styles.Bubble PtoComp;
        internal Steema.TeeChart.Styles.Points PtoGC;
        private Elegant.Ui.TabPage tabComproSol;
        internal FarPoint.Win.Spread.FpSpread spdSolucoes;
        internal FarPoint.Win.Spread.SheetView spdSolucoes_Sheet1;
        private Elegant.Ui.TabPage tabNaoDominadas;
        internal Steema.TeeChart.TChart tchGraficos;
        private Steema.TeeChart.Styles.Points Pto2D;
        private Steema.TeeChart.Styles.Points3D Pto3D;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private Elegant.Ui.NumericUpDown numSimulacao;
        internal Elegant.Ui.Label label1;
        private Steema.TeeChart.Styles.Points ptoGC2D;
    }
}
