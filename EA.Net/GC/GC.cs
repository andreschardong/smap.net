﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public class GC
    {
        public void CalcGC(Population pop, int NrObj)
        {
            double[] Max, Min;
            Max = new double[pop.Ind.Length];
            Min = new double[pop.Ind.Length];

            FuncoesGerais.ObjMaxMin(ref Max, ref Min, pop);

            int PopSize = pop.Ind.Count();
            double Deta = 2 * Math.PI / NrObj;

            pop.XComp = new double[NrObj];
            pop.YComp = new double[NrObj];

            double[,] XCp = new double[PopSize, NrObj];
            double[,] YCp = new double[PopSize, NrObj];

            for (int i = 0; i < NrObj; i++)
            {
                pop.XComp[i] = Math.Cos(Math.PI / 2 - i * Deta);
                pop.YComp[i] = Math.Sin(Math.PI / 2 - i * Deta);
            }

            double CP = 0;
            double Best = 0;
            double Worst = 0;
            double sumX = 0;
            double sumY = 0;

            for (int i = 0; i < NrObj; i++)
            {
                Best = Max[i];
                Worst = Min[i];

                for (int j = 0; j < PopSize; j++)
                {
                    CP = 2 * (Best - pop.Ind[j].Objective[i]) / (Best - Worst);
                    XCp[j, i] = -pop.XComp[i] * CP;
                    YCp[j, i] = -pop.YComp[i] * CP;
                    XCp[j, i] = Math.Round(XCp[j, i], 3);
                    YCp[j, i] = Math.Round(YCp[j, i], 3);
                }
            }

            for (int j = 0; j < PopSize; j++)
            {
                sumX = 0;
                sumY = 0;
                for (int i = 0; i < NrObj; i++)
                {
                    sumX += XCp[j, i];
                    sumY += YCp[j, i];
                }
                pop.Ind[j].XComp = sumX / NrObj;
                pop.Ind[j].YComp = sumY / NrObj;
            }
        }
    }
}
