﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net.CP
{
    public class RankComp
    {
        public void DoRank(int NrObj, int NrInd, Population pop, double[] CR, int[] Par, double[] ArrayMax, double[] ArrayMin)
        {
            int NrP = Par.Length;
            int index = 0;

            for (int i = 0; i < NrP; i++)
            {
                double par = Par[i];
                ColBase base2 = new ColBase();

                int indJ = 0;
                while (indJ < NrInd)
                {
                    double x = 0.0;
                    //int num21 = num2;
                    for (index = 0; index < NrObj; index++)
                    {
                        float sinVal = (float)CR[index];// this.cp.CR[index].SinVal;
                        float num12 = ((float)ArrayMax[index] - (float)pop.Ind[indJ].Objective[index]) / ((float)ArrayMax[index] - (float)ArrayMin[index]);
                        x += Math.Pow((double)sinVal, (double)par) * Math.Pow((double)num12, (double)par);
                    }

                    x = Math.Pow(x, (double)(1f / par));
                    base2.Add(new CBase("xx", (float)Math.Round(x, 4), indJ));
                    indJ++;
                }

                while (base2.Count > 0)
                {
                    int J = 0;
                    int intVal;
                    float fMax = float.MaxValue;

                    for (int j = 0; j < base2.Count; j++)
                    {
                        if (base2[j].SinVal < fMax)
                        {
                            fMax = base2[j].SinVal;
                            intVal = base2[j].IntVal;
                            J = j;
                        }
                    }

                    //this.cp.PR[i].DMRK[intVal].SinVal = base2[J].SinVal;
                    //this.cp.PR[i].DMRK[intVal].IntVal = (NrInd - base2.Count) + 1;
                    //this.cp.PR[i].DMRK[intVal].StrVal = String.Format(this.cp.PR[i].DMRK[intVal].SinVal, "#.000E+00") + " [" + this.cp.PR[i].DMRK[intVal].IntVal.ToString() + "]";
                    base2.Remove(J);
                }
                int y = 0;
                /*
                for (num3 = 0; num3 < NrInd; num3++)
                {
                    //this.grd[(7 + NrInd + num3, i + 1] = this.cp.PR[i].DMRK[num3].StrVal;
                }
                 */
            }
        }
    }
}