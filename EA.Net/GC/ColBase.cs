﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace EA.Net.CP
{
    [Serializable]
    public class ColBase : IEnumerable
    {
        // Fields
        private ArrayList m_col = new ArrayList();

        // Methods
        public void Add(CBase cn)
        {
            this.m_col.Add(cn);
        }

        public IEnumerator GetEnumerator()
        {
            return this.m_col.GetEnumerator();
        }

        public void Remove(int index)
        {
            this.m_col.RemoveAt(index);
        }

        public void RemoveAll()
        {
            this.m_col.Clear();
        }

        // Properties
        public int Count
        {
            get
            {
                return this.m_col.Count;
            }
        }

        public CBase this[int index]
        {
            get
            {
                return (CBase)this.m_col[index];
            }
        }
    }
}
