﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public class RND
    {
        public static Random Rnd = new Random();
        public static int rndint(int Lower, int Upper)
        {
            return Rnd.Next(Lower, Upper);
        }

        public static double rnd(double Lower, double Upper)
        {
            return Rnd.NextDouble() * (Upper - Lower) + Lower;
        }

        public static double rndDouble()
        {
            return Rnd.NextDouble();
        }

        public static int Flip(double pf)
        {
            if (Rnd.NextDouble() <= pf) return 1; else return 0;
        }
    }
}
