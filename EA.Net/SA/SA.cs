﻿
namespace EA.Net
{
    
    using System;
    using System.Collections;
         
    public class SA
    {

        private double[] Atual;
        private double[] Proxima;
        private int NumVars;
      
        public double Probabilidade;
        public double Alpha = 0.999;
        public double Temperature = 400.0;
        public double Epsilon = 0.001;

        public double[] XUpperBound;
        public double[] XLowerBound;

        public SA(int mNumVars)
        {

            NumVars = mNumVars;
            Atual = new double[mNumVars + 1];


            Proxima = new double[mNumVars + 1];
        }

        private void Assign(double[] c, double[] n)
        {
            for (int i = 0; i <= c.Length - 1; i++)
            {
                c(i) = n(i);
            }
        }

        private Random rnd = new Random();


        private void ComputeNext(double[] c, double[] n)
        {

            for (int i = 0; i <= c.Length - 1; i++)
            {
                n(i) = c(i);
            }
            int i1 = (int)rnd.Next(NumVars - 1);
            int i2 = (int)rnd.Next(NumVars - 1);
            double aux = n(i1);
            n(i1) = n(i2);

            n(i2) = aux;
        }

        public void Otimiza()
        {

            int Iteracao = -1;

            double Delta = 0;

            for (int i = 0; i <= NumVars - 1; i++)
            {
                Atual(i) = XLowerBound(i) + rnd.NextDouble * (XUpperBound(i) - XLowerBound(i));
            }

            double FO = CalculaFO(Atual);

            while (Temperature > Epsilon)
            {
                Iteracao += 1;

                ComputeNext(Atual, Proxima);
                Delta = CalculaFO(Proxima) - FO;

                if (Delta < 0)
                {
                    Assign(Atual, Proxima);
                    FO = Delta + FO;
                }
                else
                {

                    Probabilidade = rnd.Next;

                    if (Probabilidade < Math.Exp(-Delta / Temperature))
                    {
                        Assign(Atual, Proxima);
                        FO = Delta + FO;
                    }
                }


                Temperature *= Alpha;
            }



            CalculaFO(Atual);
        }

    }
    
}