﻿/* NSGA-II routine (implementation of the 'main' function) */


using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class NSGAII:Algorithm
    {
        public override void Optimize()
        {

            EA.ParentPopulation = new Population();
            EA.ChildPopulation = new Population();
            EA.MixedPopulation = new Population();

            EA.InitializePopulation();

            if (EA.EAConfig.FlagAutoProbMutation)
                EA.EAConfig.ProbMutation = 1.0 / EA.EAConfig.NumberVarsReal;

            EA.EvaluatePopulation(ref EA.ParentPopulation);
            EA.AssignRankCrowdingDistance(ref EA.ParentPopulation);

            for (int i = 0; i < EA.EAConfig.MaxGEN; i++)
            {
                Selection(EA.ParentPopulation,  ref EA.ChildPopulation);
                Mutation(EA.ChildPopulation);

                EA.EvaluatePopulation(ref EA.ChildPopulation);

                EA.Merge(EA.ParentPopulation, EA.ChildPopulation, ref EA.MixedPopulation);
                
                EA.FillNondominatedSort(EA.MixedPopulation, ref EA.ParentPopulation);

                if (EA.EAConfig.bckProcess != null)
                    EA.EAConfig.bckProcess.ReportProgress(i + 1);
            }
        }
    }
}