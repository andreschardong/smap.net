﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EA.Net
{
    public partial class NSGAII
    {
        /* Routine for tournament selection, it creates a NewPopulation from OldPopulation by performing tournament selection and the crossover */
        public void Selection(Population OldPopulation, ref Population NewPopulation)
        {
            int[] a1, a2;
            int Temp, Rand;
            Individual parent1, parent2;

            a1 = new int[EA.EAConfig.PopSize];
            a2 = new int[EA.EAConfig.PopSize];

            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                a1[i] = a2[i] = i;
            }
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                Rand = RND.rndint(i, EA.EAConfig.PopSize);
                Temp = a1[Rand];
                a1[Rand] = a1[i];
                a1[i] = Temp;

                Rand = RND.rndint(i, EA.EAConfig.PopSize);
                Temp = a2[Rand];
                a2[Rand] = a2[i];
                a2[i] = Temp;
            }
            for (int i = 0; i < EA.EAConfig.PopSize; i += 4)
            {
                parent1 = Tournament(OldPopulation.Ind[a1[i]], OldPopulation.Ind[a1[i + 1]]);
                parent2 = Tournament(OldPopulation.Ind[a1[i + 2]], OldPopulation.Ind[a1[i + 3]]);
                Crossover(parent1, parent2, ref NewPopulation.Ind[i], ref NewPopulation.Ind[i + 1]);

                parent1 = Tournament(OldPopulation.Ind[a2[i]], OldPopulation.Ind[a2[i + 1]]);
                parent2 = Tournament(OldPopulation.Ind[a2[i + 2]], OldPopulation.Ind[a2[i + 3]]);
                Crossover(parent1, parent2, ref NewPopulation.Ind[i + 2], ref NewPopulation.Ind[i + 3]);
            }
        }

        /* Routine for binary tournament */
        Individual Tournament(Individual Ind1, Individual Ind2)
        {
            int flag = EA.ChekDominace(Ind1, Ind2);

            if (flag == 1)
                return Ind1;
            else if (flag == -1)
                return Ind2;

            if (Ind1.CrowdingDistance > Ind2.CrowdingDistance)
                return Ind1;
            else if (Ind2.CrowdingDistance > Ind1.CrowdingDistance)
                return Ind2;

            if (RND.rndDouble() <= 0.5)
                return Ind1;
            else
                return Ind2;
        }
    }
}