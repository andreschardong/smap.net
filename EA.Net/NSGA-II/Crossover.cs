﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class NSGAII
    {
        /* Function to cross two individuals */
        public void Crossover(Individual parent1, Individual parent2, ref Individual child1, ref Individual child2)
        {
            if (EA.EAConfig.NumberVarsReal != 0)
                RealCrossover(parent1, parent2,ref child1,ref  child2);

            if (EA.EAConfig.NumberVarsBin != 0)
                BinCrossover(parent1, parent2, ref child1, ref child2);
        }

        /* Routine for real variable SBX crossover */
        private void RealCrossover(Individual parent1, Individual parent2,ref Individual child1, ref Individual child2)
        {
            int i;
            double rand;
            double y1, y2;
            double c1, c2;
            double alpha, beta, betaq;

            if (RND.rndDouble() <= EA.EAConfig.ProbCrossover)
            {
                //EAConfig.NumberVarscross++;
                for (i = 0; i < EA.EAConfig.NumberVarsReal; i++)
                {
                    if (RND.rndDouble() <= 0.5)
                    {
                        if (Math.Abs(parent1.Xreal[i] - parent2.Xreal[i]) > EA.EAConfig.EPS)
                        {
                            if (parent1.Xreal[i] < parent2.Xreal[i])
                            {
                                y1 = parent1.Xreal[i];
                                y2 = parent2.Xreal[i];
                            }
                            else
                            {
                                y1 = parent2.Xreal[i];
                                y2 = parent1.Xreal[i];
                            }

                            rand = RND.rndDouble();

                            beta = 1.0 + (2.0 * (y1 - EA.XLowerReal[i]) / (y2 - y1));
                            alpha = 2.0 - Math.Pow(beta, -(EA.EAConfig.IndexDistribuitionCross + 1.0));
                            
                            if (rand <= 1.0 / alpha)
                                betaq = Math.Pow(rand * alpha, (1.0 / (EA.EAConfig.IndexDistribuitionCross + 1.0)));
                            else
                                betaq = Math.Pow(1.0 / (2.0 - rand * alpha), (1.0 / (EA.EAConfig.IndexDistribuitionCross + 1.0)));
                            
                            c1 = 0.5 * (y1 + y2 - betaq * (y2 - y1));

                            beta = 1.0 + (2.0 * (EA.XUpperReal[i] - y2) / (y2 - y1));
                            alpha = 2.0 - Math.Pow(beta, -(EA.EAConfig.IndexDistribuitionCross + 1.0));
                            if (rand <= 1.0 / alpha)
                                betaq = Math.Pow(rand * alpha, 1.0 / (EA.EAConfig.IndexDistribuitionCross + 1.0));
                            else
                                betaq = Math.Pow(1.0 / (2.0 - rand * alpha), 1.0 / (EA.EAConfig.IndexDistribuitionCross + 1.0));
                            
                            c2 = 0.5 * (y1 + y2 + betaq * (y2 - y1));

                            if (c1 < EA.XLowerReal[i]) c1 = EA.XLowerReal[i];
                            else if (c1 > EA.XUpperReal[i]) c1 = EA.XUpperReal[i];

                            if (c2 < EA.XLowerReal[i]) c2 = EA.XLowerReal[i];
                            else if (c2 > EA.XUpperReal[i]) c2 = EA.XUpperReal[i];

                            if (RND.rndDouble() <= 0.5)
                            {
                                child1.Xreal[i] = c2;
                                child2.Xreal[i] = c1;
                            }
                            else
                            {
                                child1.Xreal[i] = c1;
                                child2.Xreal[i] = c2;
                            }
                        }
                        else
                        {
                            child1.Xreal[i] = parent1.Xreal[i];
                            child2.Xreal[i] = parent2.Xreal[i];
                        }
                    }
                    else
                    {
                        child1.Xreal[i] = parent1.Xreal[i];
                        child2.Xreal[i] = parent2.Xreal[i];
                    }
                }
            }
            else
            {
                for (i = 0; i < EA.EAConfig.NumberVarsReal; i++)
                {
                    child1.Xreal[i] = parent1.Xreal[i];
                    child2.Xreal[i] = parent2.Xreal[i];
                }
            }
        }

        /* Routine for two point binary crossover */
        private void BinCrossover(Individual parent1, Individual parent2, ref Individual child1, ref Individual child2)
        {
            int i;

            int temp, site1, site2;
            if (RND.rndDouble() <= EA.EAConfig.ProbCrossover)
            {
                //nbincross++;
                site1 = RND.rndint(0, EA.EAConfig.NumberVarsBin);
                site2 = RND.rndint(0, EA.EAConfig.NumberVarsBin);
                if (site1 > site2)
                {
                    temp = site1;
                    site1 = site2;
                    site2 = temp;
                }
                for (i = 0; i < site1; i++)
                {
                    child1.Xbin[i] = parent1.Xbin[i];
                    child2.Xbin[i] = parent2.Xbin[i];
                }
                for (i = site1; i < site2; i++)
                {
                    child1.Xbin[i] = parent2.Xbin[i];
                    child2.Xbin[i] = parent1.Xbin[i];
                }
                for (i = site2; i < EA.EAConfig.NumberVarsBin; i++)
                {
                    child1.Xbin[i] = parent1.Xbin[i];
                    child2.Xbin[i] = parent2.Xbin[i];
                }
            }
            else
            {
                for (i = 0; i < EA.EAConfig.NumberVarsBin; i++)
                {
                    child1.Xbin[i] = parent1.Xbin[i];
                    child2.Xbin[i] = parent2.Xbin[i];
                }
            }
        }
    }
}