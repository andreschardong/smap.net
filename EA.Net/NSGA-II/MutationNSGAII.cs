﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EA.Net
{
    public partial class NSGAII
    {

        /* Function to perform mutation in a population */
        private void Mutation(Population pop)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                RealMutateInd(ref pop.Ind[i]);
                BinMutateInd(ref pop.Ind[i]);
            }
        }

        /* Routine for real polynomial mutation of an individual */
        private void RealMutateInd(ref Individual ind)
        {
            double Rnd1, Delta1, Delta2, MutPow, DeltaQ;
            double y, yLower, yUpper, Val;

            for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
            {
                if (RND.rnd(0, 1) <= EA.EAConfig.ProbMutation)
                {
                    y = ind.Xreal[j];
                    yLower = EA.XLowerReal[j];
                    yUpper = EA.XUpperReal[j];

                    Delta1 = (y - yLower) / (yUpper - yLower);
                    Delta2 = (yUpper - y) / (yUpper - yLower);

                    Rnd1 = RND.rnd(0, 1);
                    MutPow = 1.0 / (EA.EAConfig.IndexDistribuitionMut + 1.0);

                    if (Rnd1 <= 0.5)
                    {
                        Val = 2.0 * Rnd1 + (1.0 - 2.0 * Rnd1) * (Math.Pow(1.0 - Delta1, EA.EAConfig.IndexDistribuitionMut + 1.0));
                        DeltaQ = Math.Pow(Val, MutPow) - 1.0;
                    }
                    else
                    {
                        Val = 2.0 * (1.0 - Rnd1) + 2.0 * (Rnd1 - 0.5) * (Math.Pow(1.0 - Delta2, EA.EAConfig.IndexDistribuitionMut + 1.0));
                        DeltaQ = 1.0 - Math.Pow(Val, MutPow);
                    }

                    y = y + DeltaQ * (yUpper - yLower);

                    if (y < yLower)
                        y = yLower;

                    if (y > yUpper)
                        y = yUpper;

                    ind.Xreal[j] = y;
                }
            }
        }

        /* Routine for binary mutation of an individual */
        private void BinMutateInd(ref Individual ind)
        {
            double Prob;
            for (int j = 0; j < EA.EAConfig.NumberVarsBin; j++)
            {
                Prob = RND.rndDouble();
                if (Prob <= EA.EAConfig.ProbMutation)
                {
                    if (ind.Xbin[j] == 0)
                        ind.Xbin[j] = 1;
                    else
                        ind.Xbin[j] = 0;
                }
            }
        }
    }
}