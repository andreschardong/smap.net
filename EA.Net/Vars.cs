﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Remoting.Messaging;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace EA.Net
{
    [Serializable()]
    public class Vars
    {
        public enum eAlgorithm
        {
            MoDE_NS = 0,
            MoPSO_NS = 1,
            NSGAII = 2
        }
        
        public enum eTipoModelo
        {
            Horario = 2,
            Diario = 1,
            Mensal = 0
        }

        public enum eTipoOtimizacao
        {
            DE = 0,
            GA = 1,
            PSO = 2
        }

        public enum eTipoFO
        {

            PWRMSE = 1,
            SSR = 2,
            SAR = 3,

            PEPF = 4,
            PEV = 5

        }

        public enum eCoefsVariation
        {
            Linear = 0,
            Random = 1,
            Fixo
        }

        public enum eEstrategiaDE
        {
            DE_rand_1_bin = 0,
            DE_rand_2_bin = 1,
            DE_rand_1_exp = 2,
            DE_rand_2_exp = 3,
            DE_best_1_bin = 4,
            DE_best_2_bin = 5,
            DE_best_1_exp = 6,
            DE_best_2_exp = 7,
            DE_rand_to_best_1_bin = 8,
            DE_rand_to_best_1_exp = 9,
            DE_current_to_rand_1_bin = 10,
            DE_current_to_best_1_exp = 11,
            DE_current_to_rand_2_bin = 12
        }

        public enum eStopCriteria
        {
            Geral = 0
        }

        public class ParValorTexto
        {
            //usada em conjunto com um arraylist para fazer o combobox ter um item de referencia, como no VB6

            private object Vlr;
            private string Txt;

            public object Valor
            {
                get { return Vlr; }
            }

            public string Texto
            {
                get { return Txt; }
            }

            public ParValorTexto(object NovoValor, string NovoTexto)
            {
                Vlr = NovoValor;
                Txt = NovoTexto;
            }

            public override string ToString()
            {
                return Txt;
            }

        }
    }

    public class ValorIndex : IComparable
    {

        public double Valor;
        public int Index;
        public ValorIndex(double _Valor, int _Index)
        {
            Valor = _Valor;
            Index = _Index;
        }

        public int CompareTo(object obj)
        {
            return this.Valor.CompareTo(((ValorIndex)obj).Valor);
        }

    }
}