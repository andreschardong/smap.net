﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace EA.Net
{
    public class NodeEA
    {
        public int Index;
        public NodeEA Parent;
        public NodeEA Child;
        public NodeEA()
        {
        }
    }

    public class ListEAUtil
    {
        public void Insert(ref NodeEA No, int _Index)
        {
            NodeEA temp = new NodeEA();
            temp.Index = _Index;
            temp.Child = No.Child;
            temp.Parent = No;

            if (No.Child != null)
                No.Child.Parent = temp;

            No.Child = temp;

        }

        /* Delete the node NODE from the list */
        public NodeEA Delete(NodeEA No)
        {
            NodeEA temp;
            temp = No.Parent;
            temp.Child = No.Child;
            if (temp.Child != null)
                temp.Child.Parent = temp;
            return temp;
        }
    }
}



