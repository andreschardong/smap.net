﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoDE
    {
        private Individual TrialInd;
        private Individual BestInd;
        Individual[] ListIndRank1;
        public int r0, r1, r2, r3, r4;

        public Population MoDEStrategy(Population pop)
        {
            /*
            DE_rand_1_bin = 0,
            DE_rand_2_bin = 1,
            DE_rand_1_exp = 2,
            DE_rand_2_exp = 3,
            DE_best_1_bin = 4,
            DE_best_2_bin = 5,
            DE_best_1_exp = 6,
            DE_best_2_exp = 7,
            DE_rand_to_best_1_bin = 8,
            DE_rand_to_best_1_exp = 9,
            DE_current_to_rand_1_bin = 10,
            DE_current_to_best_1_exp = 11,
             */

            Population child = new Population();
            child.Ind = new Individual[EA.EAConfig.PopSize];
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                child.Ind[i] = new Individual();
                child.Ind[i].Xreal = new double[EA.EAConfig.NumberVarsReal];
                child.Ind[i].Objective = new double[EA.EAConfig.NumberObjectives];
            }
            MoDECreateChild(pop, ref child);

            return child;
        }

        private void CreateRnd(int i)
        {
            do
                r0 = (int)Math.Floor(RND.rndDouble() * EA.EAConfig.PopSize);
            while (r0 == i);
            do
                r1 = (int)Math.Floor(RND.rndDouble() * EA.EAConfig.PopSize);
            while (r1 == i | r1 == r0);
            do
                r2 = (int)Math.Floor(RND.rndDouble() * EA.EAConfig.PopSize);
            while (r2 == i | r2 == r1 | r2 == r0);
            do
                r3 = (int)Math.Floor(RND.rndDouble() * EA.EAConfig.PopSize);
            while (r3 == i || r3 == r2 || r3 == r1 || r3 == r0);

            do
                r4 = (int)Math.Floor(RND.rndDouble() * EA.EAConfig.PopSize);
            while (r4 == i || r4 == r2 || r4 == r1 || r4 == r0);

            /*
            if (EA.EAConfig.FlagUseElitism)
            {
                Individual[] ListIndRank1;
                ListIndRank1 = (from ind in pop.Ind where ind.Rank == 1 select ind).ToArray();

                int pos1 = RND.rndint(0, ListIndRank1.Length - 1);
                int pos2 = RND.rndint(0, ListIndRank1.Length - 1);

                if (EA.ChekDominace(ListIndRank1[pos1], ListIndRank1[pos2]) >= 0)
                    BestInd = ListIndRank1[pos1];
                else
                    BestInd = ListIndRank1[pos2];
            }
            else
            {
                int pos1 = RND.rndint(0, pop.Ind.Length - 1);
                BestInd = pop.Ind[pos1];
            }

            ListIndRank1 = (from ind in EA.ParentPopulation.Ind where ind.Rank == 1 select ind).ToArray();

            int pos1 = RND.rndint(0, ListIndRank1.Length - 1);
            int pos2 = RND.rndint(0, ListIndRank1.Length - 1);

            if (EA.ChekDominace(EA.ParentPopulation.Ind[pos1], EA.ParentPopulation.Ind[pos2]) >= 0)
                BestInd = EA.ParentPopulation.Ind[pos1];
            else
                BestInd = EA.ParentPopulation.Ind[pos2];
            */
        }

        private void IniTrialVectors()
        {
            TrialInd = new Individual();
            TrialInd.Xbin = new byte[EA.EAConfig.NumberVarsReal];
            TrialInd.Xreal = new double[EA.EAConfig.NumberVarsReal];
            TrialInd.Objective = new double[EA.EAConfig.NumberObjectives];
            TrialInd.ConstraintViolation = 0;
        }

        private void NewChild(Population pop, ref Population Child, int i)
        {
            if (EA.EAConfig.FlagUseElitism)
            {
                EA.EAConfig.CalculaFO(ref TrialInd.Xbin, ref TrialInd.Xreal, ref TrialInd.Objective, ref TrialInd.ConstraintViolation);
                switch (EA.ChekDominace(pop.Ind[i], TrialInd))
                {
                    case 1:
                        EA.CopyIndividual(pop.Ind[i], ref Child.Ind[i]);
                        break;
                    case -1:
                        EA.CopyIndividual(TrialInd, ref Child.Ind[i]);
                        break;
                    case 0:
                        if (RND.rndDouble() < 0.5)
                            EA.CopyIndividual(pop.Ind[i], ref Child.Ind[i]);
                        else
                            EA.CopyIndividual(TrialInd, ref Child.Ind[i]);
                        break;
                }
            }
            else
                EA.CopyIndividual(TrialInd, ref Child.Ind[i]);
        }

        private void DE_best_1_bin(Population pop, ref Population Child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                CreateRnd(i);
                IniTrialVectors();

                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (RND.rndDouble() < _CR)
                    {      
                        TrialInd.Xreal[j] = BestInd.Xreal[j] + _F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (TrialInd.Xreal[j] > EA.XUpperReal[j])
                            TrialInd.Xreal[j] = EA.XUpperReal[j];
                        else if (TrialInd.Xreal[j] < EA.XLowerReal[j])
                            TrialInd.Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        TrialInd.Xreal[j] = pop.Ind[i].Xreal[j];
                }
                NewChild(pop, ref Child, i);
            }
        }

        private void DE_best_2_bin(Population pop, ref Population Child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                CreateRnd(i);
                IniTrialVectors();
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (RND.rndDouble() < _CR)
                    {
                        TrialInd.Xreal[j] = BestInd.Xreal[j] + _F * (pop.Ind[r0].Xreal[j] + pop.Ind[r1].Xreal[j] - pop.Ind[r2].Xreal[j] - pop.Ind[r3].Xreal[j]);

                        if (TrialInd.Xreal[j] > EA.XUpperReal[j])
                            TrialInd.Xreal[j] = EA.XUpperReal[j];
                        else if (TrialInd.Xreal[j] < EA.XLowerReal[j])
                            TrialInd.Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        TrialInd.Xreal[j] = pop.Ind[i].Xreal[j];
                }
                NewChild(pop, ref Child, i);
            }
        }

        private void DE_current_to_rand_1_bin(Population pop, ref Population Child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                CreateRnd(i);
                IniTrialVectors();

                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (RND.rndDouble() < _CR)
                    {
                        TrialInd.Xreal[j] = pop.Ind[i].Xreal[j] + _F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (TrialInd.Xreal[j] > EA.XUpperReal[j])
                            TrialInd.Xreal[j] = EA.XUpperReal[j];
                        else if (TrialInd.Xreal[j] < EA.XLowerReal[j])
                            TrialInd.Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        TrialInd.Xreal[j] = pop.Ind[i].Xreal[j];
                }
                NewChild(pop, ref Child, i);
            }
        }

        private void DE_current_to_rand_2_bin(Population pop, ref Population Child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                CreateRnd(i);
                IniTrialVectors();

                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (RND.rndDouble() < _CR)
                    {
                        TrialInd.Xreal[j] = pop.Ind[i].Xreal[j] + _F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]) + 0.5 * (pop.Ind[r2].Xreal[j] - pop.Ind[r3].Xreal[j]);

                        if (TrialInd.Xreal[j] > EA.XUpperReal[j])
                            TrialInd.Xreal[j] = EA.XUpperReal[j];
                        else if (TrialInd.Xreal[j] < EA.XLowerReal[j])
                            TrialInd.Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        TrialInd.Xreal[j] = pop.Ind[i].Xreal[j];
                }
                NewChild(pop, ref Child, i);
            }
        }

        private void DE_rand_1_bin(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (RND.rndDouble() < _CR)
                    {
                        child.Ind[i].Xreal[j] = pop.Ind[r2].Xreal[j] + _F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                }
            }
        }

        private void DE_rand_2_bin(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (RND.rndDouble() < _CR)
                    {
                        child.Ind[i].Xreal[j] = pop.Ind[r4].Xreal[j] + _F * (pop.Ind[r0].Xreal[j] + pop.Ind[r1].Xreal[j] - pop.Ind[r2].Xreal[j] - pop.Ind[r3].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                }
            }
        }
    }
}