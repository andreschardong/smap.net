﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public delegate void CreateChild(Population pop, ref Population Child);

    public partial class MoDE : Algorithm
    {
        public CreateChild MoDECreateChild;
        double _CR, _F;

        public override void Optimize()
        {
            _CR = EA.EAConfig.CR1;
            _F = EA.EAConfig.F1;

            double DeltaCR = (EA.EAConfig.CR2 - EA.EAConfig.CR1) / EA.EAConfig.MaxGEN;
            double DeltaF = (EA.EAConfig.F2 - EA.EAConfig.F1) / EA.EAConfig.MaxGEN;

            if (EA.EAConfig.CoefsVariation == Vars.eCoefsVariation.Fixo)
            {
                DeltaCR = 0; DeltaF = 0;
            }

            if (EA.EAConfig.FlagAutoProbMutation)
                EA.EAConfig.ProbMutation = 1 / EA.EAConfig.NumberVarsReal;

            EA.ParentPopulation = new Population();
            EA.ChildPopulation = new Population();
            EA.MixedPopulation = new Population();

            EA.InitializePopulation();

            EA.EvaluatePopulation(ref EA.ParentPopulation);
            EA.AssignRankCrowdingDistance(ref EA.ParentPopulation);

            if (EA.EAConfig.bckProcess != null)
                EA.EAConfig.bckProcess.ReportProgress(0);

            switch (EA.EAConfig.Strategy)
            {
                case Vars.eEstrategiaDE.DE_rand_1_bin:
                    MoDECreateChild = DE_rand_1_bin;
                    break;
                case Vars.eEstrategiaDE.DE_rand_2_bin:
                    MoDECreateChild = DE_rand_2_bin;
                    break;
                case Vars.eEstrategiaDE.DE_rand_1_exp:
                    break;
                case Vars.eEstrategiaDE.DE_rand_2_exp:
                    break;
                case Vars.eEstrategiaDE.DE_best_1_bin:
                    MoDECreateChild = DE_best_1_bin;
                    break;
                case Vars.eEstrategiaDE.DE_best_2_bin:
                    MoDECreateChild = DE_best_2_bin;
                    break;
                case Vars.eEstrategiaDE.DE_best_1_exp:
                    break;
                case Vars.eEstrategiaDE.DE_best_2_exp:
                    break;
                case Vars.eEstrategiaDE.DE_rand_to_best_1_bin:
                    break;
                case Vars.eEstrategiaDE.DE_rand_to_best_1_exp:
                    break;
                case Vars.eEstrategiaDE.DE_current_to_rand_1_bin:
                    MoDECreateChild = DE_current_to_rand_1_bin;
                    break;
                case Vars.eEstrategiaDE.DE_current_to_rand_2_bin:
                    MoDECreateChild = DE_current_to_rand_2_bin;
                    break;
            }

            for (int i = 0; i < EA.EAConfig.MaxGEN; i++)
            {
                EA.ChildPopulation = MoDEStrategy(EA.ParentPopulation);

                if (EA.EAConfig.FlagUseMutation)
                    MutateChildMoDE(EA.ChildPopulation, i);
                                
                EA.EvaluatePopulation(ref EA.ChildPopulation);

                EA.Merge(EA.ParentPopulation, EA.ChildPopulation, ref EA.MixedPopulation);
                EA.FillNondominatedSort(EA.MixedPopulation, ref EA.ParentPopulation);

                if (EA.EAConfig.bckProcess != null)
                    EA.EAConfig.bckProcess.ReportProgress(i + 1);

                if (EA.EAConfig.CoefsVariation == Vars.eCoefsVariation.Random)
                {
                    _CR = RND.rnd(EA.EAConfig.CR1, EA.EAConfig.CR2);
                    _F = RND.rnd(EA.EAConfig.F1, EA.EAConfig.F2);
                }
                else
                {
                    _CR += DeltaCR;
                    _F += DeltaF;
                }
            }
        }
    }
}