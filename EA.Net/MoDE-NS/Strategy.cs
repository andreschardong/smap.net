﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoDE 
    {
  

        /*
        private void DE_best_1_exp(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                int j = 0;
                while (j < EA.EAConfig.NumberVarsReal)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[0].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                        j++;
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                }
            }
        }

        private void DE_best_2_exp(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                int j = 0;
                while (j < EA.EAConfig.NumberVarsReal)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[0].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] + pop.Ind[r1].Xreal[j] - pop.Ind[r2].Xreal[j] - pop.Ind[r3].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];

                        j++;
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                }
            }
        }

        private void DE_rand_1_bin(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[r2].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                    //% Evaluate the objective function for the offsprings and as before
                    //% concatenate the offspring chromosome with objective value.
                }
            }
        }

        private void DE_rand_2_bin(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[r4].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] + pop.Ind[r1].Xreal[j] - pop.Ind[r2].Xreal[j] - pop.Ind[r3].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                    //% Evaluate the objective function for the offsprings and as before
                    //% concatenate the offspring chromosome with objective value.
                }
            }
        }

        private void DE_rand_to_best_1_bin(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[0].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j] - pop.Ind[0].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                    //% Evaluate the objective function for the offsprings and as before
                    //% concatenate the offspring chromosome with objective value.
                }
            }
        }

        private void DE_rand_to_best_1_exp(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                int j = 0;
                while (j < EA.EAConfig.NumberVarsReal)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[0].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j] - pop.Ind[0].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                    //% Evaluate the objective function for the offsprings and as before
                    //% concatenate the offspring chromosome with objective value.
                }
            }
        }
         
        private void DE_rand_1_exp(Population pop, ref Population child)
        {
            int j;
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                j = 0;
                while (j < EA.EAConfig.NumberVarsReal)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[r2].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                        j++;
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                    //% Evaluate the objective function for the offsprings and as before
                    //% concatenate the offspring chromosome with objective value.
                }
            }
        }
        
        private void DE_rand_2_exp(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[r4].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] + pop.Ind[r1].Xreal[j] - pop.Ind[r2].Xreal[j] - pop.Ind[r3].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                    //% Evaluate the objective function for the offsprings and as before
                    //% concatenate the offspring chromosome with objective value.
                }
            }
        }

        private void DE_current_to_rand_1_exp(Population pop, ref Population child)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                int j = 0;
                while (j < EA.EAConfig.NumberVarsReal)
                {
                
                    if (EA.rndDouble() < EA.EAConfig.CR)
                    {       // Generate the jth element of first child
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j] + EA.EAConfig.F * (pop.Ind[r0].Xreal[j] - pop.Ind[r1].Xreal[j]);

                        if (child.Ind[i].Xreal[j] > EA.XUpperReal[j])
                            child.Ind[i].Xreal[j] = EA.XUpperReal[j];
                        else if (child.Ind[i].Xreal[j] < EA.XLowerReal[j])
                            child.Ind[i].Xreal[j] = EA.XLowerReal[j];
                        j++;
                    }
                    else
                        child.Ind[i].Xreal[j] = pop.Ind[i].Xreal[j];
                }
            }
        }*/

    }
}
