﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoDE
    {

        /* Function to perform mutation in a population */
        private void MutateChildMoDE(Population pop, int Iter)
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
                //if (i % 3 == 0)
                    RealMutateInd(ref pop.Ind[i], Iter);
        }

        private void RealMutateInd(ref Individual ind, int Iter)
        {
            double DeltaQ;
            double y, yLower, yUpper;

            for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
            {

                if (RND.rndDouble() <= EA.EAConfig.ProbMutation)
                {
                    y = ind.Xreal[j];
                    yLower = EA.XLowerReal[j];
                    yUpper = EA.XUpperReal[j];

                    DeltaQ = (1.0 - Math.Pow(RND.rndDouble(), Math.Pow((1.0 - (double) Iter / EA.EAConfig.MaxGEN), 2)));
                    
                    if (RND.rndDouble() <= 0.5)
                        y += DeltaQ * (yUpper - y);
                    else
                        y += DeltaQ * (yLower-y);

                    if (y < yLower)
                        y = yLower;

                    if (y > yUpper)
                        y = yUpper;

                    ind.Xreal[j] = y;
                }
            }
        }
    }
}