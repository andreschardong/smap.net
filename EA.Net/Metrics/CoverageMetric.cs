﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class Metrics
    {
        
        public double CoverageMetric(Population front, Population trueParetoFront, int nrObj)
        {
             Util util = new Util();
            util.EPS = 0.001;

            int flag = 0;
            int sum = 0;
            bool Domina = false;
            for (int i = 0; i < trueParetoFront.Ind.Length; i++)
            {
                Domina = false;
                for (int j = 0; j <  front.Ind.Length; j++)
                {
                    flag = util.ChekDominace(trueParetoFront.Ind[i], front.Ind[j], nrObj);
                    if (flag == -1 )
                    {
                        Domina = true;
                        break;
                    }
                }
                if (Domina == true)
                    sum++;
            }
            //sum é a quantidade de individuos que dominam individos da frente
            return sum / (double)trueParetoFront.Ind.Length;
        }
    }
}
