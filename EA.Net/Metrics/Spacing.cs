﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace EA.Net
{
    public partial class Metrics
    {
        public double Spacing(Population front, int nrObj)
        {

            //double[] Max, Min;
            //Max = new double[front.Ind.Length];
            //Min = new double[front.Ind.Length];

            //FuncoesGerais.ObjMaxMin(ref Max, ref Min, front);
            //Population normalizedFront = new Population() { Ind = new Individual[front.Ind.Length] };
            //normalizedFront = GetNormalizedFront(front, Max, Min, nrObj);
            //normalizedFront = front;
            ////normalizedParetoFront = trueParetoFront;

            double[] d = new double[front.Ind.Length];
            double mean = 0.0;
            double sum = 0;

            for (int i = 0; i < front.Ind.Length - 1; i++)
            {
                d[i] = DistanceToClosedPointXY(front.Ind[i].Objective, front, i);
                //d[i] = DistanceToClosedPoint(normalizedFront.Ind[i].Objective, front, i);
                mean += d[i];
            }
            mean = mean / front.Ind.Length;

            for (int i = 0; i < front.Ind.Length - 1; i++)
                sum += Math.Pow(d[i] - mean, 2);

            return Math.Sqrt(sum / (front.Ind.Length - 1));
        }
    }
}
