﻿using System;
namespace EA.Net
{
    public partial class Metrics
    {
        public double iGD(Population front, Population trueParetoFront, int nrObj)
        {
            //double[] Max, Min;
            //Max = new double[front.Ind.Length];
            //Min = new double[front.Ind.Length];

            //FuncoesGerais.ObjMaxMin(ref Max, ref Min, trueParetoFront);
            //Population normalizedFront = new Population() { Ind = new Individual[front.Ind.Length] };
            //normalizedFront = GetNormalizedFront(front, Max, Min, nrObj);
            //Population normalizedParetoFront = new Population() { Ind = new Individual[front.Ind.Length] };
            //normalizedParetoFront = GetNormalizedFront(trueParetoFront, Max, Min, nrObj);
            //normalizedFront = front;
            //normalizedParetoFront = trueParetoFront;

            double sum = 0.0;
            for (int i = 0; i < trueParetoFront.Ind.Length; i++)
                sum += Math.Pow(DistanceToNearestPoint(trueParetoFront.Ind[i].Objective, front, nrObj), 2);

            sum = Math.Pow(sum, 0.5);

            double generationalDistance = sum / trueParetoFront.Ind.Length;

            return generationalDistance;
        }
    }
}