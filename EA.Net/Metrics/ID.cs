using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace EA.Net
{
    public partial class Metrics
    {
        public double ID(Population front, Population TrueFront, int nrObj)
        {

            double[] Max, Min;
            Max = new double[front.Ind.Length];
            Min = new double[front.Ind.Length];

            IComparer myComparer = new SortByOF();

            FuncoesGerais.ObjMaxMin(ref Max, ref Min, TrueFront);
            Population normalizedFront = new Population() { Ind = new Individual[front.Ind.Length] };
            normalizedFront = GetNormalizedFront(front, Max, Min, nrObj);

            Population normalizedParetoFront = new Population() { Ind = new Individual[front.Ind.Length] };
            normalizedParetoFront = GetNormalizedFront(TrueFront, Max, Min, nrObj);


            normalizedFront = front;
            normalizedParetoFront = TrueFront;

            double dl, df;

            int numberOfPoints = normalizedFront.Ind.Count();
            int numberOfTruePoints = normalizedParetoFront.Ind.Count();

            Array.Sort(normalizedFront.Ind, myComparer);
            Array.Sort(normalizedParetoFront.Ind, myComparer);

            df = Distance(normalizedFront.Ind[0].Objective, normalizedParetoFront.Ind[0].Objective);
            dl = Distance(normalizedFront.Ind[numberOfPoints - 1].Objective, normalizedParetoFront.Ind[numberOfTruePoints - 1].Objective);

            double mean = 0.0;
            double diversitySum = df + dl;

            for (int i = 0; i < (normalizedFront.Ind.Length - 1); i++)
                mean += Distance(normalizedFront.Ind[i].Objective, normalizedFront.Ind[i + 1].Objective);

            mean = mean / normalizedFront.Ind.Length;

            for (int i = 0; i < normalizedFront.Ind.Length - 1; i++)
                diversitySum += Math.Abs(Distance(normalizedFront.Ind[i].Objective, normalizedFront.Ind[i + 1].Objective) - mean);

            return diversitySum / (df + dl + (numberOfPoints - 1) * mean);
        }
    }
}
