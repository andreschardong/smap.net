using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace EA.Net
{
    public partial class Metrics
    {
        public class SortByOF : IComparer
        {
            int IComparer.Compare(object a, object b)
            {
                Individual a1 = (Individual)a;
                Individual b1 = (Individual)b;

                double[] c1 = a1.Objective;
                double[] c2 = b1.Objective;

                //To determine the first i, that pointOne[i] != pointTwo[i];
                int index = 0;
                while ((index < c1.Length) && (index < c2.Length) && c1[index] == c2[index])
                    index++;

                if ((index >= c1.Length) || (index >= c2.Length))
                    return 0;

                else if (c1[index] < c2[index])
                    return -1;

                else if (c1[index] > c2[index])
                    return 1;

                return 0;
            }
        }

        public class MaxMin : IComparer
        {
            int _obj;
            public MaxMin(int Obj)
            {
                _obj = Obj;
            }
            int IComparer.Compare(object a, object b)
            {
                Individual a1 = (Individual)a;
                Individual b1 = (Individual)b;

                double c1 = a1.Objective[_obj];
                double c2 = b1.Objective[_obj];

                if (c1 > c2)
                    return -1;
                else if (c1 < c2)
                    return 1;
                else
                    return 0;
            }
        }

        public double DistanceXY(double[] a, double[] b)
        {
            double Distance = 0.0;

            for (int i = 0; i < a.Length; i++)
                Distance += Math.Abs(a[i] - b[i]);

            return Distance;
        }
       
        public double Distance(double[] a, double[] b)
        {
            double Distance = 0.0;

            for (int i = 0; i < a.Length; i++)
                Distance += Math.Pow(a[i] - b[i], 2.0);

            return Math.Sqrt(Distance);
        }

        public double DistanceToClosedPoint(double[] point, Population front, int index)
        {
            double minDistance = double.MaxValue;

            for (int i = 1; i < front.Ind.Length; i++)
            {
                if (i == index)
                    continue;
                double aux = Distance(point, front.Ind[i].Objective);

                if (aux < minDistance)
                    minDistance = aux;
            }
            return minDistance;
        }

        public double DistanceToClosedPointXY(double[] point, Population front, int index)
        {
            double minDistance = double.MaxValue;

            for (int i = 1; i < front.Ind.Length; i++)
            {
                if (i == index)
                    continue;
                double aux = DistanceXY(point, front.Ind[i].Objective);
                if (aux < minDistance)
                    minDistance = aux;
            }
            return minDistance;
        }


        public double DistanceToNearestPoint(double[] point, Population front, int nrObj)
        {
            double minDistance = Double.MaxValue;

            for (int i = 0; i < front.Ind.Length; i++)
            {
                double aux = Distance(point, front.Ind[i].Objective);
                if (aux < minDistance && aux > 0.0)
                    minDistance = aux;
            }
            return minDistance;
        }

        public Population GetNormalizedFront(Population front, double[] maximumValue, double[] minimumValue, int nrObj)
        {

            Population normalizedFront = new Population() { Ind = new Individual[front.Ind.Count()] };

            for (int i = 0; i < front.Ind.Length; i++)
            {
                normalizedFront.Ind[i] = new Individual();
                normalizedFront.Ind[i].Objective = new double[nrObj];
                for (int j = 0; j < nrObj; j++)
                {
                    normalizedFront.Ind[i].Objective[j] = (front.Ind[i].Objective[j] - minimumValue[j]) / (maximumValue[j] - minimumValue[j]);
                }
            }
            return normalizedFront;
        }

        public Population InvertedFront(Population front, int nrObj)
        {
            Population invertedFront = new Population() { Ind = new Individual[front.Ind.Count()] };

            for (int i = 0; i < nrObj; i++)
            {
                invertedFront.Ind[i] = new Individual();
                invertedFront.Ind[i].Objective = new double[nrObj];
                for (int j = 0; j < nrObj; j++)
                {
                    if (front.Ind[i].Objective[j] <= 1.0 && front.Ind[i].Objective[j] >= 0.0)
                        invertedFront.Ind[i].Objective[j] = 1.0 - front.Ind[i].Objective[j];
                    else if (front.Ind[i].Objective[j] > 1.0)
                        invertedFront.Ind[i].Objective[j] = 0.0;
                    else if (front.Ind[i].Objective[j] < 0.0)
                        invertedFront.Ind[i].Objective[j] = 1.0;
                }
            }
            return invertedFront;
        }
    }
}