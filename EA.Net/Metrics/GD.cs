using System;
namespace EA.Net
{
    public partial class Metrics
    {
        public double GD(Population front, Population trueParetoFront, int nrObj)
        {
            //double[] Max, Min;
            //Max = new double[front.Ind.Length];
            //Min = new double[front.Ind.Length];

            //FuncoesGerais.ObjMaxMin(ref Max, ref Min, trueParetoFront);
            //Population normalizedFront = new Population() { Ind = new Individual[front.Ind.Length] };
            //normalizedFront = GetNormalizedFront(front, Max, Min, nrObj);
            //Population normalizedParetoFront = new Population() { Ind = new Individual[trueParetoFront.Ind.Length] };
            //normalizedParetoFront = GetNormalizedFront(trueParetoFront, Max, Min, nrObj);
            //normalizedFront = front;
            //normalizedParetoFront = trueParetoFront;
                      
            double sum = 0.0;
            for (int i = 0; i < front.Ind.Length; i++)
                sum += Math.Pow(DistanceToNearestPoint(front.Ind[i].Objective, trueParetoFront, nrObj), 2);

            return Math.Sqrt(sum) / front.Ind.Length;
        }       
    }
}