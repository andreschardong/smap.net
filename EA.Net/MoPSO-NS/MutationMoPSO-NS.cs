﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoPSO_NS
    {
        private void MutateChildMOPSO(int Iter)
        {
            Population child = EA.ChildPopulation;
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
               // if (i % 3 == 0)
                    RealMutateInd(child, i, Iter);
            }
        }

        private void RealMutateInd(Population child, int i, int Iter)
        {
            double  DeltaQ;
            double y, yLower, yUpper;
            
            for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
            {
                if (RND.rndDouble() <= EA.EAConfig.ProbMutation)
                {
                    y = Particles[i].Xreal[j];
                    yLower = EA.XLowerReal[j];
                    yUpper = EA.XUpperReal[j];

                    DeltaQ = (1.0 - Math.Pow(RND.rndDouble(), Math.Pow((1.0 - (double) Iter / EA.EAConfig.MaxGEN), 2)));

                    if (RND.rndDouble() <= 0.5)
                        y += DeltaQ * (yUpper - y);
                    else
                        y += DeltaQ * (yLower - y);

                    if (y < yLower)
                        y = yLower;

                    if (y > yUpper)
                        y = yUpper;

                    Particles[i].Xreal[j] = y;
                    child.Ind[i].Xreal[j] = y;
                }
            }
        }
    }
}
