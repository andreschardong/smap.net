﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoPSO_NS
    {
        public Population ComputeSpeedPosition(Population pop)
        {
            if (EA.EAConfig.FlagUseElitism)
            {
                Individual[] ListIndRank1;
                ListIndRank1 = (from ind in pop.Ind where ind.Rank == 1 select ind).ToArray();

                int pos1 = RND.rndint(0, ListIndRank1.Length - 1);
                int pos2 = RND.rndint(0, ListIndRank1.Length - 1);

                if (EA.ChekDominace(ListIndRank1[pos1], ListIndRank1[pos2]) >= 0)
                    pBestGlobal = ListIndRank1[pos1];
                else
                    pBestGlobal = ListIndRank1[pos2];
            }
            else
            {
                int pos1 = RND.rndint(0, pop.Ind.Length - 1);
                pBestGlobal = pop.Ind[pos1];
            }

            Population child = new Population();
            child.Ind = new Individual[EA.EAConfig.PopSize];
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                child.Ind[i] = new Individual();
                child.Ind[i].Xreal = new double[EA.EAConfig.NumberVarsReal];
                child.Ind[i].Objective = new double[EA.EAConfig.NumberObjectives];

                r1 = RND.rndDouble();
                r2 = RND.rndDouble();
  
                for (int j = 0; j < EA.EAConfig.NumberVarsReal; j++)
                {
                    Speed[i][j] = W * Speed[i][j] + C1 * r1 * (pBestLocal[i].Xreal[j] - Particles[i].Xreal[j]) + C2 * r2 * (pBestGlobal.Xreal[j] - Particles[i].Xreal[j]);

                    Particles[i].Xreal[j] = pop.Ind[i].Xreal[j] + Speed[i][j];

                    if (Particles[i].Xreal[j] > EA.XUpperReal[j])
                        Particles[i].Xreal[j] = EA.XUpperReal[j];
                    else if (Particles[i].Xreal[j] < EA.XLowerReal[j])
                        Particles[i].Xreal[j] = EA.XLowerReal[j];
                }
                EA.CopyIndividual(Particles[i], ref child.Ind[i]);
            }
            return child;
        }
    }
}