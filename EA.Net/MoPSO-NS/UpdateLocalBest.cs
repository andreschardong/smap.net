﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoPSO_NS
    {
        private void UpdateLocalBest()
        {
            Population child = EA.ChildPopulation;
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                switch (EA.ChekDominace(child.Ind[i], pBestLocal[i]))
                {
                    case 1:
                        EA.CopyIndividual(child.Ind[i], ref pBestLocal[i]);
                        break;
                    case 0:
                        if (RND.rndDouble() > 0.5)
                            EA.CopyIndividual(child.Ind[i], ref pBestLocal[i]);
                        break;
                }
            }
        }
    }
}
