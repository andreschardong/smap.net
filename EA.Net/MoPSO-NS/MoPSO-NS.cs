﻿/* NSGA-II routine (implementation of the 'main' function) */


using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EA.Net
{
    public partial class MoPSO_NS : Algorithm
    {
        Individual[] pBestLocal, Particles;
        Individual pBestGlobal;

        double r1, r2, W, C1, C2;
        double[][] Speed;


        public override void Optimize()
        {
            W = EA.EAConfig.WIni;
            C1 = EA.EAConfig.C1Ini;
            C2 = EA.EAConfig.C2Ini;

            double DeltaC1 = (EA.EAConfig.C1Fim- EA.EAConfig.C1Ini) / EA.EAConfig.MaxGEN;
            double DeltaC2 = (EA.EAConfig.C2Fim - EA.EAConfig.C2Ini) / EA.EAConfig.MaxGEN;
            double DeltaW = (EA.EAConfig.WFim - EA.EAConfig.WIni) / EA.EAConfig.MaxGEN;
            
            if (EA.EAConfig.CoefsVariation == Vars.eCoefsVariation.Fixo)
            {
                DeltaC1 = 0; DeltaC2 = 0; DeltaW = 0;
            }

            if (EA.EAConfig.FlagAutoProbMutation)
                EA.EAConfig.ProbMutation = 1.0 / EA.EAConfig.NumberVarsReal;
            
            EA.InitializePopulation();

            Particles = pBestLocal = new Individual[EA.EAConfig.PopSize];
            Speed = new double[EA.EAConfig.PopSize][];

            EA.EvaluatePopulation(ref EA.ParentPopulation);
            EA.AssignRankCrowdingDistance(ref EA.ParentPopulation);

            IniSpeedPosition();

            for (int i = 0; i < EA.EAConfig.MaxGEN; i++)
            {
                EA.ChildPopulation = ComputeSpeedPosition(EA.ParentPopulation);

                if (EA.EAConfig.FlagUseMutation)
                    MutateChildMOPSO(i);

                EA.EvaluatePopulation(ref EA.ChildPopulation);
                
                UpdateLocalBest();

                EA.Merge(EA.ParentPopulation, EA.ChildPopulation, ref EA.MixedPopulation);
                EA.FillNondominatedSort(EA.MixedPopulation, ref EA.ParentPopulation);

                if (EA.EAConfig.bckProcess != null)
                    EA.EAConfig.bckProcess.ReportProgress(i + 1);

                if (EA.EAConfig.CoefsVariation == Vars.eCoefsVariation.Random)
                {
                    W = RND.rnd(EA.EAConfig.WIni, EA.EAConfig.WFim);
                    C1 = RND.rnd(EA.EAConfig.C1Ini, EA.EAConfig.C1Fim);
                    C2 = RND.rnd(EA.EAConfig.C2Ini, EA.EAConfig.C2Fim);
                }
                else
                {
                    W += DeltaW;
                    C1 += DeltaC1;
                    C2 += DeltaC2;
                }
            }
        }

        private void IniSpeedPosition()
        {
            for (int i = 0; i < EA.EAConfig.PopSize; i++)
            {
                Speed[i] = new double[EA.EAConfig.NumberVarsReal];
                Particles[i] = pBestLocal[i] = new Individual();
                Particles[i].Objective = pBestLocal[i].Objective = new double[EA.EAConfig.NumberObjectives];
                Particles[i].Xreal = pBestLocal[i].Xreal = new double[EA.EAConfig.NumberVarsReal];
                Particles[i].Xbin = pBestLocal[i].Xbin = new byte[EA.EAConfig.NumberVarsReal];
                EA.CopyIndividual(EA.ParentPopulation.Ind[i], ref pBestLocal[i]);
                EA.CopyIndividual(EA.ParentPopulation.Ind[i], ref Particles[i]);
            }
        }
    }
}