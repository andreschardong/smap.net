﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace EA.Net
{
    [Serializable()]
    public class PSOConfig
    {
        private bool mAlterado;
        private double mC1 = 1.7;
        private double mC2 = 1.7;
        private double mW = 0.7;
        private int mNumeroParticulas = 200;
        private bool mFlagAutoNumeroParticulas = true;
        private int mMaxGEN = 200;

        public int MaxGEN
        {
            get { return mMaxGEN; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mMaxGEN); }
        }

        public bool FlagAutoNumeroParticulas
        {
            get { return mFlagAutoNumeroParticulas; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mFlagAutoNumeroParticulas); }
        }

        public int NumeroParticulas
        {
            get { return mNumeroParticulas; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mNumeroParticulas); }
        }

        public double C1
        {
            get { return mC1; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mC1); }
        }

        public double C2
        {
            get { return mC2; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mC2); }
        }

        public double W
        {
            get { return mW; }
            set { FuncoesGerais.Validar(ref mAlterado, value, ref mW); }
        }

        private delegateFO _CalculaFO;
        public delegate double delegateFO(double[] VetorX);

        [System.Xml.Serialization.XmlIgnore]
        public delegateFO CalculaFO
        {
            get
            {
                return _CalculaFO;
            }
            set
            {
                _CalculaFO = value;
            }
        }
    }

    public class PSO
    {
        private PSOConfig _PSOConfigSMAP;
        public PSOConfig PSOConfigSMAP
        {
            set { _PSOConfigSMAP = value; }
            get { return _PSOConfigSMAP; }
        }

        private List<double> _FOList = new List<double>();

        public List<double> FOlist
        {
            set { _FOList = value; }
            get { return _FOList; }
        }

        public int NumeroMaximoIteracoes;
        public int NumeroParticulas;

        public int NumeroVariaveis;
        // Numero de variáveis do problema
        public double FO_Global;
        // melhor FO Global
        public double[] FO_Local;
        //melhor FO Local
        public double[] FO;

        public double FO_Minima;
        public int FO_MinimaIndice;

        public double[] XUpper;
        public double[] XLower;

        private double[][] X;
        // posiçao
        public double[,] MelhorX_Local;
        // melhores posições
        public double[] MelhorX_Global;
        // melhores posições
        private double[,] V;
        //velocidade
        public double w;
        public double c1;
        public double c2;
        private double r1;
        private double r2;

        public Random rnd = new Random();

        public void Optimize()
        {
            BackgroundWorker bckProcess;
            bckProcess = null;

            Optimize(ref bckProcess);
        }

        public void Optimize(ref BackgroundWorker bckProcess)
        {

            double xTemp = 0;
            double vTemp = 0;

            X = new double[NumeroParticulas][];
            for (int i = 0; i <= NumeroParticulas - 1; i++)
            {
                X[i] = new double[NumeroVariaveis];
            }
            MelhorX_Local = new double[NumeroParticulas, NumeroVariaveis];
            MelhorX_Global = new double[NumeroVariaveis];

            V = new double[NumeroParticulas, NumeroVariaveis];
            FO_Local = new double[NumeroParticulas];
            FO = new double[NumeroParticulas];

            for (int i = 0; i <= NumeroParticulas - 1; i++)
            {
                for (int j = 0; j <= NumeroVariaveis - 1; j++)
                {
                    MelhorX_Local[i, j] = rnd.NextDouble() * (XUpper[j] - XLower[j]);
                    X[i][j] = MelhorX_Local[i, j];
                    V[i, j] = 0;
                }
            }

            FO_Minima = double.MaxValue;
            FO_Global = double.MaxValue;
            for (int i = 0; i <= NumeroParticulas - 1; i++)
            {
                FO_Local[i] = double.MaxValue;
            }

            for (int k = 0; k <= NumeroMaximoIteracoes - 1; k++)
            {

                RecalculaFO();
                for (int i = 0; i <= NumeroParticulas - 1; i++)
                {
                    if (FO[i] < FO_Local[i])
                    {
                        FO_Local[i] = FO[i];
                        for (int j = 0; j <= NumeroVariaveis - 1; j++)
                            MelhorX_Local[i, j] = X[i][j];

                    }
                }

                AchaMinimoFO();
                if (FO_Minima < FO_Global)
                {
                    FO_Global = FO_Minima;
                    for (int j = 0; j <= NumeroVariaveis - 1; j++)
                        MelhorX_Global[j] = X[FO_MinimaIndice][j];
                }

                for (int i = 0; i <= NumeroParticulas - 1; i++)
                {
                    for (int j = 0; j <= NumeroVariaveis - 1; j++)
                    {
                        r1 = rnd.NextDouble();
                        r2 = rnd.NextDouble();
                        vTemp = w * V[i, j] + c1 * r1 * (MelhorX_Local[i, j] - X[i][j]) + c2 * r2 * (MelhorX_Global[j] - X[i][j]);
                        xTemp = X[i][j] + vTemp;
                        V[i, j] = vTemp;

                        if (xTemp >= XLower[j] && xTemp <= XUpper[j])
                            X[i][j] = xTemp;
                    }
                }
                _FOList.Add(FO_Minima);

                if (bckProcess != null)
                    bckProcess.ReportProgress((int)(k / NumeroMaximoIteracoes * 100), k);

            }
            _PSOConfigSMAP.CalculaFO(MelhorX_Global);
        }

        private void AchaMinimoFO()
        {
            for (int i = 0; i <= NumeroParticulas - 1; i++)
            {
                if (FO[i] < FO_Minima)
                {
                    FO_Minima = FO[i];
                    FO_MinimaIndice = i;
                }
            }
        }

        private void RecalculaFO()
        {
            double FOAux = 0;
            for (int i = 0; i <= NumeroParticulas - 1; i++)
            {
                FO[i] = _PSOConfigSMAP.CalculaFO(X[i]);
                for (int j = 0; j <= NumeroVariaveis - 1; j++)
                {
                    if (X[i][j] > XUpper[j])
                        FO[i] += (X[i][j] - XUpper[j]) * 1000;
                    else if (X[i][j] < XLower[j])
                        FO[i] += (XLower[j] - X[i][j]) * 1000;
                }
            }
        }
    }
}