﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Linq;
using System.Reflection;
using System.Globalization;

namespace Updater
{
    public enum UpdateStatus : byte
    {
        NoUpdate = 0,
        UpdateFailed = 1,
        NewVersionAvailable = 2
    }

    public class UpdaterStart
    {
        public string RemoteManifest { get; set; }
        public string ExecPath { get; set; }
        public string ProgramName { get; set; }
        public Assembly CurrAS { get; set; }
        public Form frmParent { get; set; }
        public Process Processo { get; set; }
        public CultureInfo CultInfo { get; set; }

        public UpdateStatus fncCheckForUpdate()
        {
            Thread.CurrentThread.CurrentUICulture = CultInfo;

            try
            {
                Version appVersion = CurrAS.GetName().Version;
                XDocument doc = XDocument.Load(RemoteManifest);

                // if newer, display update dialog
                Version newestVersion = new Version((string)doc.Root.Element("version"));
                if (newestVersion > appVersion)
                {
                    using (frmShowUpdate f = new frmShowUpdate())
                    {
                        
                        f.Text = ProgramName + " " + appVersion;
                        f.MoreInfoLink = (string)doc.Root.Element("info");
                        f.Info = string.Format(f.Info, newestVersion, (DateTime)doc.Root.Element("date"));
                        f.Owner = frmParent;

                        if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            fncLaunchUpdater(doc);
                            Processo.Kill();
                            f.Close();
                        }
                    }
                    return UpdateStatus.NewVersionAvailable;
                }
                return UpdateStatus.NoUpdate;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            return UpdateStatus.UpdateFailed;
        }

        public void fncLaunchUpdater(XDocument manifest)
        {
            // save remote manifest locally
            string file = ExecPath.Replace(".EXE", ".exe").Replace(".exe", ".manifest");
            manifest.Save(file);

            // launch updater
            string updater = GetUpdaterPath();
            System.Diagnostics.Process.Start(updater, "\"" + file + "\" \"" + Application.ExecutablePath + "\"");
        }

        public void fncUpdateUpdater()
        {
            ThreadPool.QueueUserWorkItem((x) =>
            {
                Thread.Sleep(1000);

                // rename updater after update
                try
                {
                    string updaterPath = GetUpdaterPath();
                    string tmpUpdaterPath = updaterPath.Replace(".exe", ".exe.tmp");
                    if (File.Exists(tmpUpdaterPath))
                    {
                        File.Delete(updaterPath);
                        File.Move(tmpUpdaterPath, updaterPath);
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex);
                }
            });
        }

        private string GetUpdaterPath()
        {
            string appPath = Path.GetDirectoryName(ExecPath);
            return Path.Combine(appPath, "Updater.exe");
        }
    }
}
