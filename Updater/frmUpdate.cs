﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Linq;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using System.Threading;
using ICSharpCode.SharpZipLib.Checksums;

namespace Updater
{
    public partial class frmUpdate : Form
    {
        public string Manifest { get; set; }
        public string Executible { get; set; }
        protected string ZipFile { get; set; }
        protected bool flagUpdating { get; set; }

        public frmUpdate()
        {
            InitializeComponent();
        }

        private void frmUpdate_Load(object sender, EventArgs e)
        {
            flagUpdating = true;

            try
            {
                ZipFile = System.IO.Path.GetTempFileName();

                // read manifest
                XDocument doc = XDocument.Load(Manifest);
                string title = Path.GetFileNameWithoutExtension(Manifest);
                lblTitle.Text = string.Format(lblTitle.Text, title);
                lblInfo.Text = string.Format(lblInfo.Text, title, (string)doc.Root.Element("version"), (DateTime)doc.Root.Element("date"));
           
                // download update zip
                picBox.Image = Updater.Properties.Resources.Download;

                txtStatus.Text = "Downloading update...";
                using (var web = new WebClient())
                {
                    web.DownloadProgressChanged += new DownloadProgressChangedEventHandler((s, args) => prgBar.Value = args.ProgressPercentage);
                    web.DownloadFileCompleted += new AsyncCompletedEventHandler(web_DownloadFileCompleted);
                    web.DownloadFileAsync(new Uri((string)doc.Root.Element("download")), ZipFile);
                }
            }
            catch (Exception ex)
            {
                txtStatus.Text = "Error: failed download. Description: " + ex.Message;  
                flagUpdating = false;
            }
        }

        void web_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            System.Threading.Thread.Sleep(400); //don't go too fast
            if (e.Error != null)
            {
                Trace.WriteLine(e.Error);
                txtStatus.Text = "Error: failed download";
                flagUpdating = false;
                return;
            }

            txtStatus.Text = "Unzipping update...";
            prgBar.Value = 0;

            picBox.Image = Updater.Properties.Resources.ZipFile;

            new System.Threading.Thread(new ThreadStart(Unzip)).Start();
        }

        private void Unzip()
        {
            try
            {
                string appPath = Path.GetDirectoryName(Application.ExecutablePath);

                // unzip update
                using (var fileStream = new FileStream(ZipFile, FileMode.Open, FileAccess.Read))
                using (ZipInputStream s = new ZipInputStream(fileStream))
                {
                    ZipEntry entry;
                    while ((entry = s.GetNextEntry()) != null)
                    {
                        string path = Path.GetDirectoryName(Path.Combine(appPath, entry.Name));

                        if (entry.IsDirectory)
                        {
                            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                        }
                        else
                        {
                            //write the data to disk
                            string name = entry.Name.EndsWith("Updater.exe") ? entry.Name.Replace(".exe", ".exe.tmp") : entry.Name;
                            using (FileStream fs = File.Create(Path.Combine(appPath, name)))
                            {
                                byte[] buffer = new byte[1024];
                                int read = buffer.Length;
                                while (true)
                                {
                                    read = s.Read(buffer, 0, buffer.Length);
                                    if (read > 0) fs.Write(buffer, 0, read);
                                    else break;
                                }
                            }
                        }
                        UpdateProgress(((float)s.Position / (float)fileStream.Length) * 100F);
                        System.Threading.Thread.Sleep(400); 
                    }
                }
                UnzipFinished();
            }
            catch (Exception ex)
            {
                UnzipFailed(ex);
            }
        }

        private void UnzipFinished()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UnzipFinished));
                return;
            }

            Cleanup();
        }

        private void UnzipFailed(Exception ex)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<Exception>(UnzipFailed), ex);
                return;
            }

            Trace.WriteLine(ex); 
            txtStatus.Text = "Error: failed unzip. Message:" + ex.Message;
            flagUpdating = false;
        }

        private void UpdateProgress(float percent)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<float>(UpdateProgress), percent);
                return;
            }

            prgBar.Value = (int)percent;
        }

        private void Cleanup()
        {
            System.Threading.Thread.Sleep(400); 
            txtStatus.Text = "Cleaning up...";
            prgBar.Value = 50;
            picBox.Image = Updater.Properties.Resources.CleanUP;

            try
            {
                // delete zip
                System.IO.File.Delete(ZipFile);
                new System.Threading.Thread(new ThreadStart(Finish)).Start();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                txtStatus.Text = "Error: failed cleanup";
                flagUpdating = false;
            }
        }

        private void Finish()
        {
            if (InvokeRequired)
            {
                System.Threading.Thread.Sleep(400); //don't go too fast
                Invoke(new Action(Finish));
                return;
            }

            prgBar.Value = 100;
            txtStatus.Text = "Finished!";
            System.Threading.Thread.Sleep(400); //don't go too fast
            flagUpdating = false;  

            try
            {
                // relaunch app
                System.Diagnostics.Process.Start(Executible);

                // close
                this.Close();
            }
            catch (Exception ex)
            {
                txtStatus.Text = "Error: failed relaunch";
            }
        }

        private void frmUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (flagUpdating)
            {
                e.Cancel = MessageBox.Show("Stop the update and close?", "Update in Progress", 
                    MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes;
            }
        }

    }
}
